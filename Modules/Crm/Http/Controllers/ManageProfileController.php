<?php


namespace Modules\Crm\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Utils\ModuleUtil;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Hash;
use Carbon;

use Midtrans\Config;
use Midtrans\Snap;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

use App\Media;
use App\User;
use App\Contact;
use App\TopUp;
use App\Refund;

class ManageProfileController extends Controller
{
    /**
     * All Utils instance.
     *
     */
    protected $moduleUtil;
    /**
     * Constructor
     *
     * @param CommonUtil
     * @return void
     */
    public function __construct(ModuleUtil $moduleUtil)
    {
        $this->moduleUtil = $moduleUtil;
    }

    /**
     * Shows profile of logged in user
     *
     * @return \Illuminate\Http\Response
     */
    public function getProfile()
    {
        $business_id = request()->session()->get('user.business_id');
        if (!(auth()->user()->can('superadmin') || $this->moduleUtil->hasThePermissionInSubscription($business_id, 'crm_module'))) {
            abort(403, 'Unauthorized action.');
        }

        $user_id = request()->session()->get('user.id');
        $user = User::where('id', $user_id)->with(['media'])->first();
        $config_languages = config('constants.langs');
        $languages = [];
        foreach ($config_languages as $key => $value) {
            $languages[$key] = $value['full_name'];
        }

        return view('crm::profile.edit', compact('user', 'languages'));
    }

    public function topUpList(Request $request)
    {
        $business_id = request()->session()->get('user.business_id');
        if (!(auth()->user()->can('superadmin') || $this->moduleUtil->hasThePermissionInSubscription($business_id, 'crm_module'))) {
            abort(403, 'Unauthorized action.');
        }

        $user_id = request()->session()->get('user.id');
        $user = User::where('id', $user_id)->with(['media'])->first();
        $config_languages = config('constants.langs');
        $languages = [];
        foreach ($config_languages as $key => $value) {
            $languages[$key] = $value['full_name'];
        }

        if ($request->ajax()) {
            $sells = TopUp::where('user_id', $user_id );

            if (!empty($request->start_date) && !empty($request->end_date)) {
                $start = $request->start_date;
                $end =  $request->end_date;
                $sells->whereDate('created_at', '>=', $start)
                    ->whereDate('created_at', '<=', $end);
            }

            return Datatables::of($sells)
                ->editColumn('amount', function($item) {
                    return "Rp. ".number_format($item->amount);
                })
                ->editColumn('status', function($item) {
                    if($item->status=="Success"){
                        return '<span class="bg-success">'.$item->status.'</span>';
                    }elseif($item->status=="Pending"){
                        return '<span class="bg-warning">'.$item->status.'</span>';
                    }else{
                        return '<span class="bg-danger">'.$item->status.'</span>';
                    }
                })
                ->removeColumn('id')
                ->rawColumns(['status'])
                ->make(true);

        }

        return view('crm::topup.index');
    }

    public function topUpWallet(Request $request)
    {
        $req = $request->all();

        try{
            $user_id = request()->session()->get('user.id');
            $amount = $req['amount'];
            $TopUp = new TopUp;
            $TopUp->amount = $amount;
            $TopUp->user_id = $user_id;
            $TopUp->status = "Pending";
            $TopUp->save();
            
            $user = User::where('id', $user_id)->first();

            Config::$serverKey = config('midtrans.server_key');
            Config::$isProduction = config('midtrans.is_production');
            Config::$isSanitized = config('midtrans.is_sanitized');
            Config::$is3ds = config('midtrans.is_3ds');
    
            $params = [
                'transaction_details' => [
                    'order_id' => "Momo-".Carbon::now()->format('Ymd_Hi')."-".$user_id."-".$TopUp->id,
                    'gross_amount' => $amount,
                ],
                'item_details' => [
                    [
                        'id' => 1,
                        'price' => $amount,
                        'quantity' => 1,
                        'name' => 'Top Up Wallet Momo Beach Club',
                    ]
                ],
                'customer_details' => [
                    'first_name' => $user->first_name." ".$user->surname." ".$user->last_name,
                    'email' => $user->email,
                    'phone' => $user->contact_number,
                ]
            ];
    
            $snapToken = Snap::getSnapToken($params);
            
            $response = array(
                'status' => 'success',
                'msg' => $snapToken,
                'idTopUp' => $TopUp->id,
            );

        } catch (\Exception $e) {
            $response = array(
                'status' => 'failed',
                'msg' => $e->getMessage(),
                'idTopUp' => "",
            );
        }

        return response()->json($response); 
    }

    public function successTopUp(Request $request)
    {
        $req = $request->all();
        try{
            $user_id = request()->session()->get('user.id');
            $reqId = explode("-",$req['topup_id']);
            $topup_id = end($reqId);
            
            $topUpAmount = TopUp::where('id', (int)$topup_id)->first();

            if($topUpAmount->status !== "Success"){
                
                TopUp::where('id', (int)$topup_id)->update([
                        'status' => "Success"
                    ]);
                
                $user = User::where('id', $user_id)->first();
                $contact_id = $user->crm_contact_id;
    
                $contact = Contact::where('id', $contact_id)->first();
                $oldWallet = $contact->wallet;
    
                $newWallet = $topUpAmount->amount;
    
                $sumWallet = $oldWallet + $newWallet;
    
                Contact::where('id', $contact_id)->update([
                    'wallet' => $sumWallet
                ]);
            }

            $response = array(
                'status' => 'success',
                'msg' => "Success Top Up, Redirecting to Home",
            );
            
        } catch (\Exception $e) {
            $response = array(
                'status' => 'failed',
                'msg' => $e->getMessage(),
            );
        }

        return response()->json($response); 
    }

    public function failedTopUp(Request $request)
    {
        $req = $request->all();

        try{
            $user_id = request()->session()->get('user.id');
            $topup_id = $req['topup_id'];
            TopUp::where('id', $topup_id)->update([
                'status' => "Failed"
            ]);

            $response = array(
                'status' => 'success',
                'msg' => "Failed Top Up, Please try again later",
            );
        } catch (\Exception $e) {
            $response = array(
                'status' => 'failed',
                'msg' => $e->getMessage(),
            );
        }

        return response()->json($response); 
    }

    public function refundList(Request $request)
    {
        $business_id = request()->session()->get('user.business_id');
        if (!(auth()->user()->can('superadmin') || $this->moduleUtil->hasThePermissionInSubscription($business_id, 'crm_module'))) {
            abort(403, 'Unauthorized action.');
        }

        $user_id = request()->session()->get('user.id');
        $user = User::where('id', $user_id)->with(['media'])->first();
        $config_languages = config('constants.langs');
        $languages = [];
        foreach ($config_languages as $key => $value) {
            $languages[$key] = $value['full_name'];
        }

        if ($request->ajax()) {
            $sells = Refund::where('user_id', $user_id );

            if (!empty($request->start_date) && !empty($request->end_date)) {
                $start = $request->start_date;
                $end =  $request->end_date;
                $sells->whereDate('created_at', '>=', $start)
                    ->whereDate('created_at', '<=', $end);
            }

            return Datatables::of($sells)
                ->editColumn('amount', function($item) {
                    return "Rp. ".number_format($item->amount);
                })
                ->editColumn('status', function($item) {
                    if($item->status=="Approved"){
                        return '<span class="bg-success">'.$item->status.'</span>';
                    }elseif($item->status=="Pending"){
                        return '<span class="bg-warning">'.$item->status.'</span>';
                    }else{
                        return '<span class="bg-danger">'.$item->status.'</span>';
                    }
                })
                ->removeColumn('id')
                ->rawColumns(['status'])
                ->make(true);

        }

        return view('crm::refund.index');
    }

    public function refundWallet(Request $request)
    {
        $req = $request->all();
        try{
            $user_id = request()->session()->get('user.id');
            $user = User::where('id', $user_id)->first();
            $contact_id = $user->crm_contact_id;

            //check current wallet
            $contact = Contact::where('id', $contact_id)->first();
            $current_wallet = $contact->wallet;

            if($req['amount'] >= $current_wallet){
                $response = array(
                    'status' => 'not_enough_wallet',
                    'msg' => "Your wallet amount is not enough to Refund, current wallet = Rp.". number_format($current_wallet, 2)
                );
                return response()->json($response); 
            }
            
            //else continue request
            $amount = $req['amount'];
            $Refund = new Refund;
            $Refund->amount = $amount;
            $Refund->user_id = $user_id;
            $Refund->status = "Pending";
            $Refund->save();

            $response = array(
                'status' => 'success',
                'msg' => "success",
                'idRefund' => $Refund->id,
            );

            return response()->json($response); 
        } catch (\Exception $e) {
            $response = array(
                'status' => 'failed',
                'msg' => $e->getMessage(),
                'idRefund' => "",
            );
        }
    }

    /**
     * updates user profile
     *
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(Request $request)
    {
        $business_id = request()->session()->get('user.business_id');
        if (!(auth()->user()->can('superadmin') || $this->moduleUtil->hasThePermissionInSubscription($business_id, 'crm_module'))) {
            abort(403, 'Unauthorized action.');
        }

        try {
            $user_id = $request->session()->get('user.id');
            $input = $request->only(['surname', 'first_name', 'last_name', 'email', 'language']);

            $user = User::find($user_id);
            $user->update($input);

            Media::uploadMedia($user->business_id, $user, request(), 'profile_photo', true);

            //update session
            $input['id'] = $user_id;
            $input['business_id'] = $business_id;
            session()->put('user', $input);

            $output = ['success' => 1,
                        'msg' => __('lang_v1.profile_updated_successfully')
                    ];
        } catch (\Exception $e) {
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            $output = ['success' => 0,
                        'msg' => __('messages.something_went_wrong')
                    ];
        }

        return redirect()->back()->with(['status' => $output]);
    }
    /**
     * Update the password
     *
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(Request $request)
    {
        $business_id = request()->session()->get('user.business_id');
        if (!(auth()->user()->can('superadmin') || $this->moduleUtil->hasThePermissionInSubscription($business_id, 'crm_module'))) {
            abort(403, 'Unauthorized action.');
        }
        
        try {
            $user_id = $request->session()->get('user.id');
            $user = User::where('id', $user_id)->first();
            
            if (Hash::check($request->input('current_password'), $user->password)) {
                $user->password = Hash::make($request->input('new_password'));
                $user->save();
                $output = ['success' => 1,
                            'msg' =>  __('lang_v1.password_updated_successfully')
                        ];
            } else {
                $output = ['success' => 0,
                            'msg' => __('lang_v1.u_have_entered_wrong_password')
                        ];
            }
        } catch (\Exception $e) {
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            $output = ['success' => 0,
                            'msg' => __('messages.something_went_wrong')
                        ];
        }
        return redirect()->back()->with(['status' => $output]);
    }
}
