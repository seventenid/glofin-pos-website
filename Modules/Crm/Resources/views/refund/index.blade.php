@extends('crm::layouts.app')

@section('title', 'Refund List')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header no-print">
   <h1>Refund List</h1>
</section>
<!-- Main content -->
<section class="content no-print">
    <div class="row" style="margin-bottom:30px">
        <div class="col-md-6">
            <div class="form-group row">
                {!! Form::label('amount', 'Amount to Refund:*', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-9">
                    <div class="input-group">
                        <span class="input-group-addon">
                            IDR
                        </span>
                        <input type="hidden" name="amount" id="amount" class="form-control">
                        <input type="text" required name="amount_mask" id="amount_mask" class="form-control">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group row">
                <div class="col-sm-6 control-label">
                    <button  onclick="showValue()" class="btn btn-primary" id="pay-button">Request Refund</button>
                </div>
                <div class="col-sm-6">
                </div>
            </div>
        </div>
    </div>
    @component('components.filters', ['title' => __('report.filters')])
        <div class="col-md-3">
            <div class="form-group">
                {!! Form::label('date_range_filter', __('report.date_range') . ':') !!}
                {!! Form::text('date_range_filter', null, ['placeholder' => __('lang_v1.select_a_date_range'), 'class' => 'form-control', 'readonly']); !!}
            </div>
        </div>
        
    @endcomponent
	@component('components.widget', ['class' => 'box-primary', 'title' => "Refund List"])
        <div class="table-responsive">
            <table class="table table-bordered table-striped ajax_view" id="topup_table">
                <thead>
                    <tr>
                        <th>@lang('messages.date')</th>
                        <th>Amount</th>
                        <th>@lang('sale.payment_status')</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr class="bg-gray font-17 footer-total text-center">
                    </tr>
                </tfoot>
            </table>
        </div>
    @endcomponent
</section>
@endsection
@section('javascript')
<script>
    $(document).ready(function(){
        var elem = document.getElementById("amount_mask");

        elem.addEventListener("keydown",function(event){
            
            var $input = $(this);
            $input.val($input.val().replace(/[^\d]+/g,''));
        });

        elem.addEventListener("keyup",function(event){
            var $input = $(this);
            $input.val($input.val().replace(/[^\d]+/g,''));

            var value = this.value.replace(/,/g,"");
            this.dataset.currentValue=parseInt(value);
            var caret = value.length-1;
            while((caret-3)>-1)
            {
                caret -= 3;
                value = value.split('');
                value.splice(caret+1,0,",");
                value = value.join('');
            }
            this.value = value;
        });
    });    

    function showValue()
    {
        document.getElementById("amount").value = document.getElementById("amount_mask").dataset.currentValue;
    }
</script>

<script type="text/javascript">
	$(document).ready(function() {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $("#pay-button").click(function(){
            $.ajax({
                /* the route pointing to the post function */
                url: '/contact/refund-wallet',
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: {_token: CSRF_TOKEN, amount:$("#amount").val()},
                dataType: 'JSON',
                /* remind that 'data' is the response of the AjaxController */
                success: function (data) { 
                    if(data.status == "success"){
                        toastr.success("Success Requesting Refund");
                        contact_sell_datatable.ajax.reload();
                    }else if(data.status == "not_enough_wallet"){
                        toastr.warning(data.msg);
                    }else{
                        toastr.error("Failed to connect to server, Please try again or contact administrator");
                    }
                }
            }); 
        });

        $('#date_range_filter').daterangepicker(
            dateRangeSettings,
            function (start, end) {
                $('#date_range_filter').val(start.format(moment_date_format) + ' ~ ' + end.format(moment_date_format));
                contact_sell_datatable.ajax.reload();
            }
        );

        $('#date_range_filter').on('cancel.daterangepicker', function(ev, picker) {
            $('#date_range_filter').val('');
            contact_sell_datatable.ajax.reload();
        });

		contact_sell_datatable = $("#topup_table").DataTable({
            processing: true,
            serverSide: true,
            aaSorting: [[1, 'desc']],
            "ajax": {
                "url": "/contact/refund-list",
                "data": function ( d ) {
                    if($('#date_range_filter').val()) {
                        var start = $('#date_range_filter').data('daterangepicker').startDate.format('YYYY-MM-DD');
                        var end = $('#date_range_filter').data('daterangepicker').endDate.format('YYYY-MM-DD');
                        d.start_date = start;
                        d.end_date = end;
                    }
                    d = __datatable_ajax_callback(d);
                }
            },
            columns: [
                { data: 'created_at', name: 'created_at'  },
                { data: 'amount', name: 'amount'},
                { data: 'status', name: 'status'},
            ],
            "fnDrawCallback": function (oSettings) {
            },
            createdRow: function( row, data, dataIndex ) {
            }
        });

	});
</script>
@endsection