<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{

    protected $table = 'blog';
    protected $guarded = ['id'];

    public $timestamps = false;


    // public function scopeFilter($query, $filters)
    // {
    //     $query->when($filters['search'] ?? false, function($query, $search){
    //         return $query->where(function($query) use ($search){
    //             $query->where('title', 'like', '%' . $search . '%')
    //             ->orWhere('content', 'like', '%' . $search . '%');
    //         });
    //     });
    // }
}
