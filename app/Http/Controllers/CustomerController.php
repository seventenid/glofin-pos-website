<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use DateTimeZone;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Validator;
use Session;
use App\Utils\Util;
use App\Utils\ContactUtil;
use App\Utils\ModuleUtil;
use App\Utils\BusinessUtil;
use Modules\Crm\Utils\CrmUtil;
use App\Utils\TransactionUtil;
use App\System;
use App\Contact;
use App\BusinessLocation;
use App\Transaction;
use App\TransactionSellLine;
use App\Printer as Printers;

use Spatie\Browsershot\Browsershot;
use Illuminate\Support\Str;

use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\ImagickEscposImage;
use Mike42\Escpos\PrintConnectors\RawbtPrintConnector;
use Mike42\Escpos\CapabilityProfile;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;

class CustomerController extends Controller
{
    protected $commonUtil;
    protected $contactUtil;
    protected $businessUtil;
    protected $mailDrivers;
    protected $crmUtil;
    protected $moduleUtil;
    protected $transactionUtil;

    public function __construct(BusinessUtil $businessUtil, ModuleUtil $moduleUtil, CrmUtil $crmUtil, ContactUtil $contactUtil, Util $commonUtil, TransactionUtil $transactionUtil)
    {
        $this->commonUtil = $commonUtil;
        $this->contactUtil = $contactUtil;
        $this->businessUtil = $businessUtil;
        $this->crmUtil = $crmUtil;
        $this->transactionUtil = $transactionUtil;
        $this->moduleUtil = $moduleUtil;
        
        $this->theme_colors = [
            'blue' => 'Blue',
            'black' => 'Black',
            'purple' => 'Purple',
            'green' => 'Green',
            'red' => 'Red',
            'yellow' => 'Yellow',
            'blue-light' => 'Blue Light',
            'black-light' => 'Black Light',
            'purple-light' => 'Purple Light',
            'green-light' => 'Green Light',
            'red-light' => 'Red Light',
        ];

        $this->mailDrivers = [
                'smtp' => 'SMTP',
                // 'sendmail' => 'Sendmail',
                // 'mailgun' => 'Mailgun',
                // 'mandrill' => 'Mandrill',
                // 'ses' => 'SES',
                // 'sparkpost' => 'Sparkpost'
            ];
    }

    public function getRegister()
    {
        $currencies = $this->businessUtil->allCurrencies();
        
        $timezone_list = $this->businessUtil->allTimeZones();

        $months = [];
        for ($i=1; $i<=12; $i++) {
            $months[$i] = __('business.months.' . $i);
        }

        $accounting_methods = $this->businessUtil->allAccountingMethods();
        $package_id = request()->package;

        $system_settings = System::getProperties(['superadmin_enable_register_tc', 'superadmin_register_tc'], true);
        
        return view('auth.register_customer', compact(
            'currencies',
            'timezone_list',
            'months',
            'accounting_methods',
            'package_id',
            'system_settings'
        ));
    }

    public function postRegister(Request $request)
    {
        try {
            $business_id = env('FESTIVAL_ID', 0);  //will change to real account momo bussiness id
            if($business_id){
                $input = $request->only(['first_name', 'password', 'password_confirmation', 'mobile', 'email']);

                $validator = Validator::make($request->all(), [
                    'mobile' => 'required|min:5|max:50|unique:users,contact_number',
                    'password' => 'required|confirmed|min:8',
                ]);
    
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                }
    
                $inputContact['name'] = $input['first_name'];
                $inputContact['first_name'] = $input['first_name'];
                $inputContact['type'] = "customer";
                $inputContact['mobile'] = $input['mobile'];
                $inputContact['email'] = $input['email'];
    
                if (!empty($input['dob'])) {
                    $inputContact['dob'] = $this->commonUtil->uf_date($input['dob']);
                }
    
                $inputContact['business_id'] = $business_id;
                $inputContact['created_by'] = 107; //will change to real account admin momo
    
                $inputContact['credit_limit'] = null;
                $inputContact['opening_balance'] = 0;
                $inputContact['wallet'] = null;
    
                DB::beginTransaction();
                
                $output = $this->contactUtil->createNewContact($inputContact);
    
                $this->moduleUtil->getModuleData('after_contact_saved', ['contact' => $output['data'], 'input' => $request->input()]);
    
                $this->contactUtil->activityLog($output['data'], 'added');
    
                //MODIFIED CODE FOR CONTACT LOGIN
                $input_user = $request->only( 'email', 'first_name', 'password');
                $input_user['status'] = 'active';
                $input_user['username'] = $input['mobile'];
                $input_user['first_name'] = $input['first_name'];
                $input_user['business_id'] = $business_id;
                $input_user['allow_login'] = 1;
    
                $input_user['crm_contact_id'] = $output['data']->id;
                $input_user['contact_number'] = $input['mobile'];
                $input_user['surname'] = null;
                $input_user['alt_number'] = null;
                $input_user['family_number'] = null;
                $user = $this->crmUtil->creatContactPerson($input_user);
                //MODIFIED CODE FOR CONTACT LOGIN
    
                DB::commit();
    
                $output = ['success' => 1,
                        'msg' => "Successfully Register"
                    ];
    
                return redirect('login')->with('status', $output);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());

            $output = ['success' => 0,
                            'msg' => __('messages.something_went_wrong')
                        ];

            return back()->with('status', $output)->withInput();
        }

    }
    
    function my_escapeshellarg($input)
    {
      $input = str_replace('\'', '\\\'', $input);

      return '\''.$input.'\'';
    }

    //html RawBt Print
    public function htmlRawBtPrint(Request $request)
    {
        $profile = CapabilityProfile::load("POS-5890");
        /* Fill in your own connector here */
        $connector = new RawbtPrintConnector();
        $printer = new Printer($connector, $profile);
        try {

            $business_id = request()->session()->get('user.business_id');
            $user_id = request()->session()->get('user.id');

            $path = public_path('print');
            $blade_file = 'slim2';
            $stringsavefile = $business_id."(".$user_id.")_invoice.png";

            /* Print it */
            /*$img = EscposImage::load($path.'/'.$stringsavefile, false);
            if ($profile->getSupportsGraphics()) {
                $printer->graphics($img);
            }
            if ($profile->getSupportsBitImageRaster() && !$profile->getSupportsGraphics()) {
                $printer->bitImage($img);
            }*/
            
            $pages = ImagickEscposImage::loadPdf($path.'/'.$stringsavefile, 260);
            foreach ($pages as $page) {
                if ($profile->getSupportsGraphics()) {
                    $printer->graphics($page);
                }
                if ($profile->getSupportsBitImageRaster() && !$profile->getSupportsGraphics()) {
                    $printer->bitImage($page);
                }
            }

            /* Cut the receipt and open the cash drawer */
            $printer->cut();
            $printer->pulse();
        } catch (\Exception $e) {
            dd($e->getMessage());
        } finally {
            $printer->close();
        }
    }   

    //Checker RawBt Print
    public function checkerRawBtPrint(Request $request)
    {
        $profile = CapabilityProfile::load("POS-5890");
        $connector = new RawbtPrintConnector();
        $printer = new Printer($connector, $profile);
        try {
            $business_id = request()->session()->get('user.business_id');
            $user_id = request()->session()->get('user.id');

            $path = public_path('print');
            $stringsavefile = $business_id."(".$user_id.")_checker.png";
            
            $pages = ImagickEscposImage::loadPdf($path.'/'.$stringsavefile, 260);
            foreach ($pages as $page) {
                if ($profile->getSupportsGraphics()) {
                    $printer->graphics($page);
                }
                if ($profile->getSupportsBitImageRaster() && !$profile->getSupportsGraphics()) {
                    $printer->bitImage($page);
                }
            }

            $printer->cut();
            $printer->pulse();
        } catch (\Exception $e) {
            dd($e->getMessage());
        } finally {
            $printer->close();
        }
    } 

    //Checker Print
    public function checkerPrint($transaction_id, $business_location)
    {
        try {
            $user_id = request()->session()->get('user.id');
            $business_id = request()->session()->get('user.business_id');

            //get printer configured
            $printerConfigured = BusinessLocation::where('id', $business_location)->first();
            $printerConfiguredType = $printerConfigured->receipt_printer_type;
            $printerConfiguredID = $printerConfigured->printer_id;
            $printerConfiguredList = Printers::where('id', $printerConfiguredID)->first();

            $path = public_path('print');   
            $output['checker_content']  = null;

            if($printerConfiguredType=="browser"){
                return "browser";
            }

            $receipt_contents = $this->transactionUtil->getPdfContentsForGivenTransaction($business_id, $transaction_id);
            $receipt_details = $receipt_contents['receipt_details'];
            $output['checker_content'] = view('sale_pos.receipts.checker1', compact('receipt_details'))->render();

            if($output['checker_content']){
                $stringsavefile = $path.'/'.$business_id."(".$user_id.")_checker.png";
                $htmlsavefile = $path.'/'.$business_id.'('.$user_id.')_checker.html';
                if(\File::exists( $stringsavefile )){
                    \File::delete( $stringsavefile );
                }

                $options = [
                    'width' => 550,
                    'quality' => 90
                ];
                if(isset($printerConfiguredList->connection_type)){
                    if($printerConfiguredList->connection_type=="bluetooth"){
                        if($printerConfiguredList->capability_profile=="TEP-200M"){
                            $options = [
                                'width' => 550,
                                'quality' => 90
                            ];
                        }else{
                            $options = [
                                'width' => 400,
                                'quality' => 90
                            ];
                        }
                    }
                }
                  
                $convChecker = new \Anam\PhantomMagick\Converter();
                //$convChecker->setBinary('C:/phantomjs/bin/phantomjs.exe');
                //$convChecker->make($output['checker_content'])->toPng($options)->save($path."/".$stringsavefile);

                \File::put($htmlsavefile, $output['checker_content']);
                $convChecker->make($htmlsavefile)->toPng($options)->save($stringsavefile);
            }
            
            if(isset($printerConfiguredList->connection_type)){
                if($printerConfiguredList->connection_type=="network" || $printerConfiguredList->connection_type=="bluetooth"){
                    if($printerConfiguredList->connection_type=="bluetooth"){
                        return "bluetooth";
                    }
                    
                    return "printer";
                    /*
                    $profile = CapabilityProfile::load($printerConfiguredList->capability_profile ?? "simple");
                    $connector = new NetworkPrintConnector($printerConfiguredList->ip_address, $printerConfiguredList->port);
                    $printer = new Printer($connector, $profile);
                    $pages = ImagickEscposImage::loadPdf($stringsavefile, 260);
                    foreach ($pages as $page) {
                        if ($profile->getSupportsGraphics()) {
                            $printer->graphics($page);
                        }
                        if ($profile->getSupportsBitImageRaster() && !$profile->getSupportsGraphics()) {
                            $printer->bitImage($page);
                        }
                    }

                    $printer->cut();
                    $printer->pulse();
                    $printer->close();
                    */
                }
            }
           
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    } 

    //Invoice RawBt Print
    public function invoiceRawBtPrint(Request $request)
    {
        // \Log::emergency("print invoiceRawBtPrint");
        //start print
        $profile = CapabilityProfile::load("POS-5890");
        $connector = new RawbtPrintConnector();
        $printer = new Printer($connector, $profile);
        try {

            $business_id = request()->session()->get('user.business_id');
            $user_id = request()->session()->get('user.id');

            $path = public_path('print');
            $stringsavefile = $business_id."(".$user_id.")_invoice.png";
            
            $pages = ImagickEscposImage::loadPdf($path.'/'.$stringsavefile, 260);
            foreach ($pages as $page) {
                if ($profile->getSupportsGraphics()) {
                    $printer->graphics($page);
                }
                if ($profile->getSupportsBitImageRaster() && !$profile->getSupportsGraphics()) {
                    $printer->bitImage($page);
                }
            }

            $printer->cut();
            $printer->pulse();
        } catch (\Exception $e) {
            dd($e->getMessage());
        } finally {
            $printer->close();
        }
    } 

    //Invoice Print
    public function invoicePrint($transaction_id, $business_location)
    {
        try {
            $user_id = request()->session()->get('user.id');
            $business_id = request()->session()->get('user.business_id');

            //get printer configured
            $printerConfigured = BusinessLocation::where('id', $business_location)->first();
            $printerConfiguredType = $printerConfigured->receipt_printer_type;
            $printerConfiguredID = $printerConfigured->printer_id;
            $printerConfiguredList = Printers::where('id', $printerConfiguredID)->first();

            $path = public_path('print');   
            $output['invoice_content']  = null;

            if($printerConfiguredType=="browser"){
                return "browser";
            }

            $receipt_contents = $this->transactionUtil->getPdfContentsForGivenTransaction($business_id, $transaction_id);
            $receipt_details = $receipt_contents['receipt_details'];
            
            if($printerConfiguredList->capability_profile=="TEP-200M"){
                $output['invoice_content'] = view('sale_pos.receipts.slim3', compact('receipt_details'))->render();
            }else{
                $output['invoice_content'] = view('sale_pos.receipts.slim2', compact('receipt_details'))->render();
            }

            if($output['invoice_content']){
                $stringsavefile = $path.'/'.$business_id."(".$user_id.")_invoice.png";
                $htmlsavefile = $path.'/'.$business_id.'('.$user_id.')_invoice.html';
                if(\File::exists( $stringsavefile )){
                    \File::delete( $stringsavefile );
                }

                $options = [
                    'width' => 550,
                    'quality' => 90
                ];
                if(isset($printerConfiguredList->connection_type)){
                    if($printerConfiguredList->connection_type=="bluetooth"){
                        $options = [
                            'width' => 400,
                            'quality' => 90
                        ];
                        if($printerConfiguredList->capability_profile=="TEP-200M"){
                            $options = [
                                'width' => 550,
                                'quality' => 90
                            ];
                        }
                    }
                }
                  
                $convChecker = new \Anam\PhantomMagick\Converter();

                \File::put($htmlsavefile, $output['invoice_content']);
                $convChecker->make($htmlsavefile)->toPng($options)->save($stringsavefile);
            }
            
            if(isset($printerConfiguredList->connection_type)){
                if($printerConfiguredList->connection_type=="network" || $printerConfiguredList->connection_type=="bluetooth"){
                    if($printerConfiguredList->connection_type=="bluetooth"){
                        return "bluetooth";
                    }

                    return "printer";
                    /*
                    $profile = CapabilityProfile::load($printerConfiguredList->capability_profile ?? "simple");
                    $connector = new NetworkPrintConnector($printerConfiguredList->ip_address, $printerConfiguredList->port);
                    $printer = new Printer($connector, $profile);
                    $pages = ImagickEscposImage::loadPdf($stringsavefile, 260);
                    foreach ($pages as $page) {
                        if ($profile->getSupportsGraphics()) {
                            $printer->graphics($page);
                        }
                        if ($profile->getSupportsBitImageRaster() && !$profile->getSupportsGraphics()) {
                            $printer->bitImage($page);
                        }
                    }

                    $printer->cut();
                    $printer->pulse();
                    $printer->close();
                    */
                }
            }
           
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    } 

    public function checkslim2($transaction_id)
    {
        $business_id = request()->session()->get('user.business_id');

        $receipt_contents = $this->transactionUtil->getPdfContentsForGivenTransaction($business_id, $transaction_id);
        $receipt_details = $receipt_contents['receipt_details'];
        $location_details = $receipt_contents['location_details'];
        $is_email_attachment = false;

        return view('sale_pos.receipts.slim2', compact('receipt_details', 'location_details', 'is_email_attachment'));
    }

    public function checkchecker1($transaction_id)
    {
        $business_id = request()->session()->get('user.business_id');

        $receipt_contents = $this->transactionUtil->getPdfContentsForGivenTransaction($business_id, $transaction_id);
        $receipt_details = $receipt_contents['receipt_details'];
        $location_details = $receipt_contents['location_details'];
        $is_email_attachment = false;

        return view('sale_pos.receipts.checker1', compact('receipt_details', 'location_details', 'is_email_attachment'));
    }

    public function checkdetailed($transaction_id)
    {
        $business_id = request()->session()->get('user.business_id');

        $receipt_contents = $this->transactionUtil->getPdfContentsForGivenTransaction($business_id, $transaction_id);
        $receipt_details = $receipt_contents['receipt_details'];
        $location_details = $receipt_contents['location_details'];
        $is_email_attachment = false;

        return view('sale_pos.receipts.detailed', compact('receipt_details', 'location_details', 'is_email_attachment'));
    }

    public function checkersa($transaction_id)
    {
        $business_id = request()->session()->get('user.business_id');

        $receipt_contents = $this->transactionUtil->getPdfContentsForGivenTransaction($business_id, $transaction_id);
        $receipt_details = $receipt_contents['receipt_details'];
        $location_details = $receipt_contents['location_details'];
        $is_email_attachment = false;

        return view('sale_pos.receipts.ersa', compact('receipt_details', 'location_details', 'is_email_attachment'));
    }


    public function checkSaldo($code = null)
    {
        if($code){
            $codes = str_replace("https:glofinpos.comsaldo","https://glofinpos.com/saldo/", $code);
            $codess = "https://glofinpos.com/saldo/".$codes;
            $user = Contact::where('mobile', $codess)->first();

            if(!$user){
                return view('checksaldo');
            }else{
                return view('checksaldo', compact('user'));
            }
        }else{
            return view('checksaldo');
        }
    }

    public function checkSaldoProcess($code)
    {
        try{
            $codes = str_replace("https:glofinpos.comsaldo","https://glofinpos.com/saldo/", $code);
            $user = Contact::where('mobile', $codes)->first();
            $output ="error";
            if(!$user){
                $output ="error";
            }else{
                $output = [
                    'name' => $user->name,
                    'balance' => $user->wallet ? number_format($user->wallet) : 0
                ];
            }
            return $output;
        } catch (\Exception $e) {
            $output ="error";
            return $output;
        }
    }
}
