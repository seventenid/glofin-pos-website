<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class staticController extends Controller
{
    public function blog()
    {
        return view('static.blog', [
            'blog' => Blog::all()
        ]);
    }


    public function faq(){
        return view('static.faq');
    }

    public function feature(){
        return view('static.feature');
    }

    public function package_device(){
        return view('static.package_device');
    }

    public function package_subscribe(){
        return view('static.package_subscribe');
    }
    public function notification(){
        return view('static.notification');
    }
}
