<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TopUp extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $table = 'top_ups';
    
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */

    public function user()
    {
        return $this->belongsTo(\App\User::class, 'user_id');
    }
}
