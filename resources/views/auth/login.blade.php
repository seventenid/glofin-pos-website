<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <title>GlofinPOS | Login Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('login_page/fonts/icomoon/style.css') }}">

    <link rel="stylesheet" href="{{ asset('login_page/css/owl.carousel.min.css') }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('login_page/css/bootstrap.min.css') }}">
    
    <!-- Style -->
    <link rel="stylesheet" href="{{ asset('login_page/css/style.css') }}">
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{ asset('landing/img/favicon.png') }}">

  </head>
  <body style="background-image: url({{ asset('images/bg-home.png') }});">
  
  <div class="content" >
    <div class="container">
      <div class="row">
        <div class="col-md-3">
        </div>
        <div class="col-md-6 contents" >
          <div class="row justify-content-center">
            <div>
              <img src="{{ asset('images/logo_login.png') }}" alt="" style="width: 200px; padding-bottom: 10%">
            </div>
            <div class="col-md-8" style="background-color: aliceblue; padding-bottom: 5%; padding-top: 5%; border-radius: 5%">
              <div class="mb-4">
              <h3 style="text-align: center">Sign In</h3>
              <p class="mb-4" style="text-align: center">Sign in now to use Glofin POS system</p>
            </div>
            <form method="POST" action="{{ route('login') }}" id="login-form">
                {{ csrf_field() }}
              <div class="form-group first">
                <label for="username">Username</label>
                <input id="username" type="text" class="form-control" name="username" value="{{ $username ?? '' }}" required autofocus placeholder="Username ">
                @if ($errors->has('username'))
                    <span class="help-block" style="color:red">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                @endif
              </div>
              <div class="form-group last mb-4">
                <label for="password">Password</label>
                <input id="password" type="password" class="form-control" name="password"
                value="{{ $password ?? '' }}" required placeholder="@lang('lang_v1.password')">
                @if ($errors->has('password'))
                    <span class="help-block" style="color:red">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
              </div>
              
              <div class="d-flex mb-5 align-items-center">
                <span class="ml-auto"><a href="{{ route('password.request') }}" class="forgot-pass">Forgot Password</a></span> 
              </div>

              <input type="submit" value="Log In" class="btn btn-block btn-primary" style="color: white; background-color: #319dcb; border: none;">
            </form>
            </div>
          </div>
          
        </div>
        <div class="col-md-3">
        </div>
      </div>
    </div>
  </div>

  
    <script src="{{ asset('login_page/js/jquery-3.3.1.min.js?v=' . $asset_v) }}"></script>
    <script src="{{ asset('login_page/js/popper.min.js?v=' . $asset_v) }}"></script>
    <script src="{{ asset('login_page/js/bootstrap.min.js?v=' . $asset_v) }}"></script>
    <script src="{{ asset('login_page/js/main.js?v=' . $asset_v) }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#change_lang').change( function(){
                window.location = "{{ route('login') }}?lang=" + $(this).val();
            });

            $('a.demo-login').click( function (e) {
            e.preventDefault();
            $('form#login-form').submit();
            });
        })
    </script>
  </body>
</html>