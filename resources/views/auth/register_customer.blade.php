@extends('layouts.auth')
@section('title', 'Register Customer')

@section('content')

<div class="row" style="margin-top:80px">

    <h1 class="page-header text-center">Register and Get Started in minutes</h2>
    
    <div class="col-md-8 col-md-offset-2">
        
        <div class="box box-solid">

            <form method="POST" action="{{ route('customer.postRegister') }}" id="customer-register-form">
            {{ csrf_field() }}

                <!-- /.box-header -->
                <div class="box-body">
                    @php
                        $first_name = old('first_name');
                        $email = old('email');
                        $mobile = old('mobile');
                        $password = null;
                        $password_confirmation = null;
                    @endphp
                    <div class="form-group has-feedback {{ $errors->has('first_name') ? ' has-error' : '' }}">
                        <input id="first_name" type="text" class="form-control" name="first_name" value="{{ $first_name }}" required autofocus placeholder="Input Your Name (*)">
                        <span class="fa fa-user form-control-feedback"></span>
                        @if ($errors->has('first_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('first_name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                        <input id="email" type="email" class="form-control" name="email" value="{{ $email }}" autofocus placeholder="Email">
                        <span class="fa fa-user form-control-feedback"></span>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group has-feedback {{ $errors->has('mobile') ? ' has-error' : '' }}">
                        <input id="mobile" type="text" class="form-control" name="mobile" value="{{ $mobile }}" required autofocus placeholder="Mobile Phone, Required for login (Number Only) (*)">
                        <span class="fa fa-user form-control-feedback"></span>
                        @if ($errors->has('mobile'))
                            <span class="help-block">
                                <strong>{{ $errors->first('mobile') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">
                        <input id="password" type="password" class="form-control" name="password"
                        value="{{ $password }}" required placeholder="@lang('lang_v1.password') min 8 character (*)">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <input id="password_confirmation" type="password" class="form-control" name="password_confirmation"
                        value="{{ $password_confirmation }}" required placeholder="Password Confirmation (*)">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        @if ($errors->has('password_confirmation '))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation ') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
                <!-- /.box-body -->
                
                <div class="box-footer">
                    <button type="submit" class="btn btn-success pull-right">Register</button>
                </div>

            </form>
            
        </div>
          <!-- /.box -->
    </div>

</div>

@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        $('#change_lang').change( function(){
            window.location = "{{ route('RegisterCustomer') }}?lang=" + $(this).val();
        });

        var elem = document.getElementById("mobile");
        elem.addEventListener("keyup",function(event){
            var $input = $(this);
            $input.val($input.val().replace(/[^\d]+/g,''));
        });

    })
</script>
@endsection