@extends('blog.layouts.main')

@section('content')

<form method="POST" action="/dashboard/blog" enctype="multipart/form-data">
    @csrf
    <div class="mb-3">
        <label for="title" class="form-label">Title</label>
        <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" value="{{ old('title') }}">
        @error('title')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
    </div>
    <input type="hidden" name="excerp">
    <div class="mb-3">
        <label for="image" class="form-label">Blog Image</label>
        <input class="form-control @error('image') is-invalid @enderror" type="file" id="image" name="image" onchange="previewImage()">
        <img src="" class="img-preview img-fluit my-3 col-sm-5 " alt="">
        @error('image')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="mb-3">
        <label for="content" class="form-label">Content</label>
        @error('content')
            <p class="text-danger">{{ $message }}</p>
        @enderror
        <input id="content" type="hidden" name="content">
        <trix-editor input="content"></trix-editor>
    </div>
    <button type="submit" class="btn btn-primary">Simpan</button>
    
    <script>

        document.addEventListener('trix-file-accept', function(e){
            e.preventDefault();
        })

        function previewImage(){
                const image = document.querySelector('#image');
                const imagePreview = document.querySelector('.img-preview')

                imagePreview.style.display = 'block';

                const oFReader = new FileReader();
                oFReader.readAsDataURL(image.files[0]);

                oFReader.onload = function(oFREvent){
                    imagePreview.src = oFREvent.target.result;
                }
            }
    </script>

</form>

@endsection