@extends('blog.layouts.main')


@section('content')

  <a href="/dashboard/blog/create" class="btn btn-primary my-3">Tambah data</a>
  @if (session()->has('success'))
      <div class="alert alert-success">
        {{ session('success') }}
      </div>
  @endif
  <div class="table-responsive">
      <table class="table table-striped table-sm">
        <thead>
          <tr>
            <th scope="col">No</th>
            <th scope="col">Title</th>
            <th scope="col">content</th>
            <th scope="col">action</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($blog as $blogs)    
            <tr>
              <td>{{ $loop->iteration }}</td>
              <td>{{ $blogs->title }}</td>
              <td>{!! $blogs->content !!}</td>
              <td>
                <a href="/dashboard/blog/{{ $blogs->id }}" class="badge bg-info d-inline">detail</a>
                <a href="/dashboard/blog/{{ $blogs->id }}/edit" class="badge bg-warning d-inline">edit</a>
                <form action="/dashboard/blog/{{ $blogs->id }}" class="d-inline" method="POST">
                  @method('delete')
                  @csrf
                  <button class="badge bg-danger " onclick="alert('Anda yakin untuk menghapus?')">Delete</button>
                </form>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
@endsection