<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" dir="{{in_array(session()->get('user.language', config('app.locale')), config('constants.langs_rtl')) ? 'rtl' : 'ltr'}}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>GlofinPOS - {{ Session::get('business.name') }}</title>
        
        @include('layouts.partials.css')
        <link rel="stylesheet" href="{{ URL::asset('css/main_code.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('css/responsive_code.css') }}">
        <style>
            .drawingBuffer {
                position: absolute;
                top: 0;
                left: 0;
            }

            button{
                margin-top: 20px;
                margin-bottom: 20px;
                padding:10px;
                background-color: initial;
                background-image: linear-gradient(-180deg, #00D775, #00BD68);
                border-radius: 5px;
                box-shadow: rgba(0, 0, 0, 0.1) 0 2px 4px;
                color: #FFFFFF;
                cursor: pointer;
                display: inline-block;
                font-family: Inter,-apple-system,system-ui,Roboto,"Helvetica Neue",Arial,sans-serif;
                outline: 0;
                overflow: hidden;
                pointer-events: auto;
                position: relative;
                text-align: center;
                touch-action: manipulation;
                user-select: none;
                -webkit-user-select: none;
                vertical-align: top;
                white-space: nowrap;
                width: 80%;
                z-index: 9;
                border: 0;
            }
            button:hover {
                background: #00bd68;
            }
            #html5-qrcode-button-camera-stop{
                background-image: linear-gradient(-180deg, #d70000, #e70831);
            }
            #html5-qrcode-button-camera-stop:hover {
                background: #d70000;
            }
            #qr-reader__scan_region img{
                width: 200px !important;
            }
            ul li{
                display: flex;
                justify-content: space-between;
                padding-bottom: 10px;
                margin-bottom: 10px;
                border-bottom: 1px solid #DAE1E7;
            }
            #qr-reader__dashboard_section{
                color: white !important;
            }
            ::placeholder{
                color: white !important;
            }
            .banner-text{
                padding-top: 20px !important;
            }
            .btnsearch{
                width: 30%;
                height: 60px;
                display: block;
                padding: 0 10px;
                background-color: #03A9F4;
                border: 1px solid #03A9F4;
                color: #fff;
                -webkit-border-top-right-radius: 2px;
                -webkit-border-bottom-right-radius: 2px;
                -moz-border-radius-topright: 2px;
                -moz-border-radius-bottomright: 2px;
                border-top-right-radius: 2px;
                border-bottom-right-radius: 2px;
                font-weight: 700;
                transition: all 0.125s ease-in-out 0s;
                -moz-transition: all 0.125s ease-in-out 0s;
                -webkit-transition: all 0.125s ease-in-out 0s;
                -o-transition: all 0.125s ease-in-out 0s;
                -ms-transition: all 0.125s ease-in-out 0s;
                float: right;
                cursor: pointer;
            }
        </style>
    </head>

    <body class="banner-area">
        <!--header section -->
        <section class="banner" role="banner">
            <!-- overlay -->
            <div class="banner-area-gradient"></div>
            <!-- overlay -->
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-12" style="margin:auto">
                            <div class="banner-text text-center"> <a class="logo" href="#">
                                <img style="width:300px;" src="{{ URL::asset('images/logotix.png') }}">
                            </a></div>

                            <div class="scanner" style="display:none">
                                <div id="qr-reader" style="width:100%; margin:auto; border-radius: 20px;"></div>
                            </div>
                        </div>
                        <!--subscribe section -->
                        <div id="subscribes" class="subscribe" style="width:100% !important">
                            <!-- subscribe form -->
                            @if(!isset($user))
                            <div class="subscribe-form">
                                <h2>Search by Booking Code</h2>
                                <form name="subscribeform" id="subscribeform">
                                    <input style="color: white;" type="text" name="email" class="input-result" id="qr-reader-results"  placeholder="Enter QR Code / Code here" id="subemail" />
                                    <input type="button" name="send" value="Search" id="subsubmit" class="btn2 btnsearch" onClick="checkTicket()" />
                                </form>
                            </div>
                            @endif

                            @if(isset($user))
                                <div class="subscribe-form" id="show-data" style="display:block">
                                    <h1 style="color: white" id="welcomename">Welcome </br>{{$user->name}}</h1>
                                </div>
                                <div class="subscribe-form" id="show-data2" style="display:block;">
                                    <h3 style="color: white">Your Current Balance is : Rp.<span id="balance">{{number_format($user->wallet)}}</span></h3>
                                </div>
                            @else
                                <div class="subscribe-form" id="show-data" style="display:none">
                                    <h1 style="color: white" id="welcomename"></h1>
                                </div>
                                <div class="subscribe-form" id="show-data2" style="display:none;">
                                    <h3 style="color: white">Your Current Balance is : Rp.<span id="balance"></span> </h3>
                                </div>
                            @endif
                         </div>
                        <!--subscribe section -->
                    </div>
                </div>
            </div>

            <!-- Footer section -->
            <footer class="footer">
                <div class="container">
                    <div class="row">
                    <div class="col-md-12">
                        <p> Copyright © 2023 <a href="https://tixzy.id">Tixzy.id</p>
                    </div>
                    </div>
                </div>
            </footer>
        <!-- Footer section -->
        </section>
        <!--header section -->
    </body>

    <script src="{{ asset('js/vendor.js?v=' . $asset_v) }}"></script>
    @if(file_exists(public_path('js/lang/' . session()->get('user.language', config('app.locale')) . '.js')))
        <script src="{{ asset('js/lang/' . session()->get('user.language', config('app.locale') ) . '.js?v=' . $asset_v) }}"></script>
    @else
        <script src="{{ asset('js/lang/en.js?v=' . $asset_v) }}"></script>
    @endif
    <script src="{{ URL::asset('js/jquery.backstretch.min.js') }}"></script>
    <script src="{{ URL::asset('js/main.js') }}"></script>
    <script src="{{ URL::asset('js/html-qrcode.js') }}"></script>
    <script>
    function docReady(fn) {
        // see if DOM is already available
        if (document.readyState === "complete"
            || document.readyState === "interactive") {
            // call on next available tick
            setTimeout(fn, 1);
        } else {
            document.addEventListener("DOMContentLoaded", fn);
        }
    }

    docReady(function () {
        var resultContainer = document.getElementById('qr-reader-results');
        var lastResult, countResults = 0;
        function onScanSuccess(decodedText, decodedResult) {
            //if (decodedText !== lastResult) {
                ++countResults;
                lastResult = decodedText;
                // Handle on success condition with the decoded message.
                checkTicket(decodedText);
            //}
        }

        var html5QrcodeScanner = new Html5QrcodeScanner(
            "qr-reader", { fps: 10, qrbox: 250 });
        html5QrcodeScanner.render(onScanSuccess);
    });

    $('#qr-reader-results').focus();
    $('.qr-reader-results').focusout(function(){
         $('.qr-reader-results').focus();
    });

    $('#subscribeform').on('submit', function(e){
        e.preventDefault();
        ticketcode = $('#qr-reader-results').val();
        checkTicket(ticketcode);
    });

    function checkTicket(ticketcode){
        console.log(ticketcode);
        if(!ticketcode){
            ticketcode = $('#qr-reader-results').val();
        }

        var codes = ticketcode.replaceAll('/','');

        var url = '{{ route("customer.checkSaldoProcess", ":id") }}';
        url = url.replace(':id', codes );
        $.ajax({
            url: url,
            type: 'get',
            dataType: 'json', // added data type
            success: function(data) {
                $('#welcomename').text("Welcome "+data['name']);
                $('#balance').text(data['balance']);

                $("#show-data").css("display", "block");
                $("#show-data2").css("display", "block");
                
                $('#qr-reader-results').val("");
                $('#qr-reader-results').focus();

                window.setInterval(resetdata, 5000);
            },
            error: function(data){
                $("#show-data").css("display", "block");
                $('#welcomename').text("Please Try Again!");
                $('#qr-reader-results').val("");
                $('#qr-reader-results').focus();
            }
        });
    }

    function resetdata() { 
        $('#welcomename').text("");
        $('#balance').text("");

        $("#show-data").css("display", "none");
        $("#show-data2").css("display", "none");
        $('#qr-reader-results').val("");
        $('#qr-reader-results').focus();
    }

    function addCommas(nStr)
    {
        if(nStr>0){
            nStr += '';
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        }else{
            return 0;
        }
    }

    function markCompleted(){
    }

    $('#modal-booking-1').on('hidden.bs.modal', function () {
        $('#qr-reader-results').val("");
        $('#qr-reader-results').focus();
    });

    $(document).keydown(function(event) {
        if (event.keyCode == 27) {
            $('#modal-booking-1').modal('toggle');
        }
    });

    $('#modal-booking-1').on('show.bs.modal', function () {
        $('#markascompleted').focus();
    });

    $(document).on('keypress',function(e) {
        if(e.which == 13) {
            if($('#modal-booking-1').data('bs.modal')?._isShown){
                if($('#markascompleted').is(":hidden")){
                    //markCompleted()
                    $('#modal-booking-1').modal('toggle');
                }else{
                    markCompleted()
                }
            }
        }
    });
    </script>
</html>