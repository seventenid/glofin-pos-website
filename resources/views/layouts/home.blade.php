<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>GlofinPOS | Home</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Swiper slider-->
    <link rel="stylesheet" href="{{ asset('landing/vendor/swiper/swiper-bundle.min.css') }}">
    <!-- Modal Video-->
    <link rel="stylesheet" href="{{ asset('landing/vendor/modal-video/css/modal-video.min.css') }}">
    <!-- Google fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:400,600,800&amp;display=swap">
    <!-- Device Mockup-->
    <link rel="stylesheet" href="{{ asset('landing/css/device-mockups.css') }}">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{ asset('landing/css/style.default.css') }}" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{ asset('landing/css/custom.css') }}">
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{ asset('landing/img/favicon.png') }}">

    <style>
        .heroo {
            background: url("{{ asset('landing/img/banner-4.png') }}") no-repeat; 
            background-size: 100% 80%;
        }
    </style>
</head>
  </head>
  <body>
    <!-- navbar-->
    <header class="header">
      <nav class="navbar navbar-light navbar-expand-lg fixed-top" id="navbar">
        <div class="container"><a class="navbar-brand" href="index.html"><img src="{{ asset('images/Logo-01.png') }}" alt="" width="110"></a>
          <button class="navbar-toggler navbar-toggler-end" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="fas fa-bars"></i></button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto">
              <li class="nav-item"><a class="btn btn-primary btn-lg px-4" href="{{ route('login') }}"> <i class="fa-solid fa-arrow-right-to-bracket"></i>@lang('lang_v1.login')</a></li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
    <!-- Hero Section-->
    <section class="hero bg-top py-5 heroo" id="hero">
      <div class="container py-5">
        <div class="row py-5">
          <div class="col-lg-5 py-5">
            <h1>Jinah Bali by Glofin POS</h1>
            <p class="my-4 text-muted">Brand new balinesse Emoney provided by Global Informa indonesia, it can be used on all of our merchant all around Bali Island</p>
          </div>
          <div class="col-lg-6 ml-auto" style="margin-top:50px">
            <div class="device-wrapper mx-auto">
            <div class="device" data-device="SurfacePro3" data-orientation="landscape" data-color="black" style="z-index: 10">
                <div class="screen">
                    <img style="width: 100%; height: 100%; border-radius:25px" src="{{ asset('images/bg-emoney.png') }}">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="bg-center py-0" id="about" style="background: url(img/service-bg.svg) no-repeat; background-size: cover">
      <section class="about py-0">
        <div class="container">
          <h2 class="mb-5">Glofin POS, your best POS application</h2>
          <div class="row pb-5 gy-4">
            <div class="col-lg-4 col-md-6">
              <!-- Services Item-->
              <div class="card border-0 shadow rounded-lg py-4 text-start">
                <div class="card-body p-5">
                  <svg class="svg-icon svg-icon-light" style="width:60px;height:60px;color:#ff904e">
                    <use xlink:href="#document-saved-1"> </use>
                  </svg>
                  <h3 class="h4 my-4">Easy to Use</h3>
                  <p class="text-sm text-muted mb-0">All of our Apps feature designed by profesionals to cover all of your bussiness needs</p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6">
              <!-- Services Item-->
              <div class="card border-0 shadow rounded-lg py-4 text-start">
                <div class="card-body p-5">
                  <svg class="svg-icon svg-icon-light" style="width:60px;height:60px;color:#39f8d2">
                    <use xlink:href="#map-marker-1"> </use>
                  </svg>
                  <h3 class="h4 my-4">Cloud Service</h3>
                  <p class="text-sm text-muted mb-0">All of your data will save through cloud service, worry no more to lost your bussiness data</p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6">
              <!-- Services Item-->
              <div class="card border-0 shadow rounded-lg py-4 text-start">
                <div class="card-body p-5">
                  <svg class="svg-icon svg-icon-light" style="width:60px;height:60px;color:#8190ff">
                    <use xlink:href="#arrow-target-1"> </use>
                  </svg>
                  <h3 class="h4 my-4">Market</h3>
                  <p class="text-sm text-muted mb-0">Know your market before selling, using this apps you can track down where all of your buyer come form</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </section>
    <footer class="with-pattern-1 position-relative pt-5">
      <div class="container py-5">
        <div class="row gy-4">
          <div class="col-lg-3"><img class="mb-4" src="img/logo.svg" alt="" width="110">
            <p class="text-muted">Glofin POS is SaaS application to cover your bussiness growth, come grow your business with us</p>
          </div>
          <div class="col-lg-2">
          </div>
          <div class="col-lg-2">
          </div>
          <div class="col-lg-5">
            <h2 class="h5 mb-4">Contact Info</h2>
            <ul class="list-unstyled me-4 mb-3">
              <li class="mb-2 text-muted">Denpasar, Bali, Indonesia </li>
              <li class="mb-2"><a class="footer-link" href="tel:6281338770043">+6281338770043</a></li>
              <li class="mb-2"><a class="footer-link" href="mailto:Info.globalinformaindonesia@gmail.com">Info.globalinformaindonesia@gmail.com</a></li>
            </ul>
            <ul class="list-inline mb-0">
              <li class="list-inline-item"><a class="social-link" href="https://www.facebook.com/"><i class="fab fa-facebook-f"></i></a></li>
              <li class="list-inline-item"><a class="social-link" href="https://www.instagram.com/glofinpos/?hl=en"><i class="fab fa-instagram"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="copyrights">       
        <div class="container text-center py-4">
          <p class="mb-0 text-muted text-sm">&copy; 2022, Glofin POS by Global Informa Indonesia.</p>
        </div>
      </div>
    </footer>

    
    @include('layouts.partials.javascripts')
    <script src="{{ asset('js/login.js?v=' . $asset_v) }}"></script>
    @yield('javascript')
    <!-- JavaScript files-->
    <script src="{{ asset('landing/vendor/bootstrap/js/bootstrap.bundle.min.js?v=' . $asset_v) }}"></script>
    <script src="{{ asset('landing/vendor/swiper/swiper-bundle.min.js?v=' . $asset_v) }}"></script>
    <script src="{{ asset('landing/vendor/modal-video/js/modal-video.js?v=' . $asset_v) }}"></script>
    <script src="{{ asset('landing/js/front.js?v=' . $asset_v) }}"></script>
    <script>
      // ------------------------------------------------------- //
      //   Inject SVG Sprite - 
      //   see more here 
      //   https://css-tricks.com/ajaxing-svg-sprite/
      // ------------------------------------------------------ //
      function injectSvgSprite(path) {
      
          var ajax = new XMLHttpRequest();
          ajax.open("GET", path, true);
          ajax.send();
          ajax.onload = function(e) {
          var div = document.createElement("div");
          div.className = 'd-none';
          div.innerHTML = ajax.responseText;
          document.body.insertBefore(div, document.body.childNodes[0]);
          }
      }
      // this is set to BootstrapTemple website as you cannot 
      // inject local SVG sprite (using only 'icons/orion-svg-sprite.svg' path)
      // while using file:// protocol
      // pls don't forget to change to your domain :)
      injectSvgSprite('https://bootstraptemple.com/files/icons/orion-svg-sprite.svg'); 
      
    </script>
    <!-- FontAwesome CSS - loading as last, so it doesn't block rendering-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  </body>
</html>