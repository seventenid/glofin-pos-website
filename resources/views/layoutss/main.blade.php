<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9]><html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>

    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <title>Glofinpos</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Favicons -->
    <link rel="shortcut icon" href="{{ asset('content/favicon.png') }}">

    <!-- FONTS -->
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:100,300,400,400italic,500,700,700italic,900'>
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Patua+One:100,300,400,400italic,700'>
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,900'>

    <!-- CSS -->
    <link rel='stylesheet' href='{{ asset('css/css/css/global.css') }}'>
    <link rel='stylesheet' href='{{ asset('css/css/css/structure.css') }}'>
    <link rel='stylesheet' href='{{ asset('css/css/css/seo3.css') }}'>
    <link rel='stylesheet' href='{{ asset('css/css/global.css') }}'>
    <link rel='stylesheet' href='{{ asset('content/app6/css/structure.css') }}'>
    <link rel='stylesheet' href='{{ asset('content/app6/css/app6.css') }}'>
    <link rel='stylesheet' href='{{ asset('content/app6/css/custom.css') }}'>

    <!-- Google tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-237620293-1">
    </script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-237620293-1');
    </script>

    

</head>

<body class="home page button-round layout-full-width one-page no-content-padding header-transparent minimalist-header-no sticky-header sticky-tb-color ab-hide subheader-both-center menu-line-below-80-1 menuo-right menuo-no-borders logo-no-margin footer-copy-center mobile-tb-center mobile-side-slide mobile-mini-mr-lc tablet-sticky mobile-header-mini mobile-sticky">
    <div id="Wrapper">

        @include('layoutss.partials.navbar')

        <div id="Content">
           @yield('content')
        </div>
        
        
    <!-- Messenger Plugin Obrolan Code -->
    <div id="fb-root"></div>

    <!-- Your Plugin Obrolan code -->
    <div id="fb-customer-chat" class="fb-customerchat">
    </div>

        <footer id="Footer" class="clearfix">
            <div class="footer_copy">
                <div class="container">
                    <div class="column one">
                        <div class="copyright">
                            &copy; {{ date('Y') }} - Global Informa Indoensia - Glofinpos System   
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <div id="Side_slide" class="right dark" style="background: #0c5d80" data-width="250">
        <div class="close-wrapper">
            <a href="#" class="close"><i class="icon-cancel-fine"></i></a>
        </div>
        <div class="extras">
            <div class="extras-wrapper"></div>
        </div>
        <div class="menu_wrapper"></div>
        <a href="{{ '/login' }}" style="text-align: center; width: 25%; background: #319dcb; color: black; margin-left: 40%; padding: 5%; border-radius: 10%">Login</a>
    </div>
    <div id="body_overlay"></div>

    <!-- JS -->
    <script src="{{ asset('js/js/jquery-3.6.0.min.js') }}"></script>
	<script src="{{ asset('js/js/jquery-migrate-3.3.2.js') }}"></script>

    <script src="{{ asset('js/js/mfn.menu.js') }}"></script>
    <script src="{{ asset('js/js/jquery.plugins.js') }}"></script>
    <script src="{{ asset('js/js/jquery.jplayer.min.js') }}"></script>
    <script src="{{ asset('js/js/animations/animations.js') }}"></script>
    <script src="{{ asset('js/js/translate3d.js') }}"></script>
    <script src="{{ asset('js/js/scripts.js') }}"></script>

    <script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>

    <script>
      var chatbox = document.getElementById('fb-customer-chat');
      chatbox.setAttribute("page_id", "111911074959197");
      chatbox.setAttribute("attribution", "biz_inbox");
    </script>

    <!-- Your SDK code -->
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          xfbml            : true,
          version          : 'v15.0'
        });
      };

      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/id_ID/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    </script>
   
</body>

</html>