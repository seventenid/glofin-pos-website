        <div id="Header_wrapper">
            <header id="Header">
                <div id="Top_bar">
                    <div class="container">
                        <div class="column one">
                            <div class="top_bar_left clearfix">
                                <div class="logo">
                                    <a id="logo" href="{{ '/' }}" title="" data-height="60" data-padding="30"><img style="max-height: 60px !important;" class="logo-main scale-with-grid" src="{{ asset('content/app6/images/logo.png') }}" data-retina="{{asset('content/app6/images/logo.png')}}" data-height="55"></a>
                                </div>
                                <div class="menu_wrapper">
                                    <nav id="menu">
                                        <ul id="menu-main-menu" class="menu menu-main">
                                            <!-- <li>
                                                <a><span>Our Package</span></a>
                                                <ul class="drop">
                                                    <li style="background-color: #fff000; ">
                                                        <a href="{{ '/package_subscribe' }}" style="color: #319dcb;"><span>Subcription Package</span></a>
                                                    </li>
                                                    <li style="background-color: #fff000;">
                                                        <a href="{{ '/package_device' }}" style="color: #319dcb;"><span>Device Package</span></a>
                                                    </li>
                                                </ul>
                                            </li> -->
                                            <li>
                                                <a href="{{ '/feature' }}"><span>Features</span></a>
                                            </li>
                                            <li>
                                                <a href="{{ '/' }}#app"><span>Contact Us</span></a>
                                            </li>
                                            <li>
                                                <a href="{{ '/blog' }}"><span>Blog</span></a>
                                            </li>
                                            <li>
                                                <a href="{{ '/faq' }}"><span>FAQ</span></a>
                                            </li>
                                        </ul>
                                    </nav><a class="responsive-menu-toggle" href="#"><i class="icon-menu-fine"></i></a>
                                </div>
                            </div>
                            
                            <div class="top_bar_right">
                                <div class="top_bar_right_wrapper">
                                    <a href="{{ '/login' }}" class="action_button">SIGN IN <i class="icon-right-open"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        </div>