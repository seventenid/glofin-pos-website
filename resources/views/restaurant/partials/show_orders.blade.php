


	@forelse($orders as $order)
		<div class="col-md-3 col-xs-6 order_div">
			<div class="small-box bg-gray">
				<div class="inner">
					<h4 class="text-center">#{{$order->invoice_no}}</h4>
					<table class="table no-margin no-border table-slim">
						<tr><th>@lang('restaurant.placed_at')</th><td>{{@format_date($order->created_at)}} {{ @format_time($order->created_at)}}</td></tr>
						<tr><th>@lang('restaurant.order_status')</th>
								@php
										$count_sell_line = count($order->sell_lines);
										$count_cooked = count($order->sell_lines->where('res_line_order_status', 'cooked'));
										$count_served = count($order->sell_lines->where('res_line_order_status', 'served'));
										$order_status =  'received';
										if($count_cooked == $count_sell_line) {
											$order_status =  'cooked';
										} else if($count_served == $count_sell_line) {
											$order_status =  'served';
										} else if ($count_served > 0 && $count_served < $count_sell_line) {
											$order_status =  'partial_served';
										} else if ($count_cooked > 0 && $count_cooked < $count_sell_line) {
											$order_status =  'partial_cooked';
										}
										
								@endphp
								<td><span class="label @if($order_status == 'cooked' ) bg-red @elseif($order_status == 'served') bg-green @elseif($order_status == 'partial_cooked') bg-orange @else bg-light-blue @endif">@lang('restaurant.order_statuses.' . $order_status) </span></td>
							</tr>
						<tr><th>@lang('contact.customer')</th><td>{{$order->customer_name}}</td></tr>
						<tr><th>@lang('restaurant.table')</th><td>{{$order->table_name}}</td></tr>
						<tr><th>@lang('sale.location')</th><td>{{$order->business_location}}</td></tr>
					</table>

					@if($order->sell_lines)
					<table class="table no-border table-slim" style="margin-top:20px;margin-bottom:5px">
						<tr><td>
							<b>Item:</b>
						</td></tr>
						@foreach($order->sell_lines as $sell_line)
							@if($sell_line->product)
								@if($sell_line->product->category)
									
								@if($modules=="Kitchen")
									@if( strpos($sell_line->product->category->name, 'Bar') == false && strpos($sell_line->product->category->name, 'Drink') == false )
										<tr><td>
											<div class="custom-control checkbox-xl custom-checkbox" >
												<input type="checkbox" class="custom-control-input cooked_item" id="{{$sell_line->id}}"  data-href="{{action('Restaurant\BarController@lineCooked', [$sell_line->id])}}" style="height:15px;width:15px" {{$sell_line->is_cooked==1 ? 'checked':''}} >
												<label class="custom-control-label" for="{{$sell_line->id}}" style="font-size:14px">
													{{$sell_line->product->name ?? ''}} ( {{$sell_line->quantity ?? ''}} ) </br>
												</label>
												@if($sell_line->sell_line_note)
													</br><span style="font-size:12px;font-weight:normal">{{$sell_line->sell_line_note ?? ''}}</span>
												@endif
											</div>
										</td></tr>
									@endif
								@elseif($modules=="Bar")
									@if( strpos($sell_line->product->category->name, 'Bar') !== false && strpos($sell_line->product->category->name, 'Drink') !== false )
										<tr><td>
											<div class="custom-control checkbox-xl custom-checkbox" >
												<input type="checkbox" class="custom-control-input cooked_item"  data-href="{{action('Restaurant\BarController@lineCooked', [$sell_line->id])}}" id="{{$sell_line->id}}" style="height:15px;width:15px" {{$sell_line->is_cooked==1 ? 'checked':''}} >
												<label class="custom-control-label" for="{{$sell_line->id}}" style="font-size:14px">
													{{$sell_line->product->name ?? ''}} ( {{$sell_line->quantity ?? ''}} )
												</label>
												
												@if($sell_line->sell_line_note)
													</br><span style="font-size:12px;font-weight:normal">{{$sell_line->sell_line_note ?? ''}}</span>
												@endif
											</div>
										</td></tr>
									@endif
								@else
									<tr><td>
										<div class="custom-control checkbox-xl custom-checkbox" >
											<input type="checkbox" class="custom-control-input cooked_item"  data-href="{{action('Restaurant\BarController@lineCooked', [$sell_line->id])}}" id="{{$sell_line->id}}" style="height:15px;width:15px" {{$sell_line->is_cooked==1 ? 'checked':''}} >
											<label class="custom-control-label" for="{{$sell_line->id}}" style="font-size:14px">
												{{$sell_line->product->name ?? ''}} ( {{$sell_line->quantity ?? ''}} )
											</label>
											
											@if($sell_line->sell_line_note)
												</br><span style="font-size:12px;font-weight:normal">{{$sell_line->sell_line_note ?? ''}}</span>
											@endif
										</div>
									</td></tr>
								@endif

								@endif
							@endif
						@endforeach
					</table>
					@endif

				</div>
				@if($orders_for == 'kitchen' || $orders_for == 'bar')
					<a href="#" class="btn btn-flat small-box-footer bg-yellow mark_as_cooked_btn" data-href="{{action('Restaurant\KitchenController@markAsCooked', [$order->id])}}"><i class="fa fa-check-square-o"></i> @lang('restaurant.mark_as_cooked')</a>
				@elseif($orders_for == 'waiter' && $order->res_order_status != 'served')
					<a href="#" class="btn btn-flat small-box-footer bg-yellow mark_as_served_btn" data-href="{{action('Restaurant\OrderController@markAsServed', [$order->id])}}"><i class="fa fa-check-square-o"></i> @lang('restaurant.mark_as_served')</a>
				@else
					<div class="small-box-footer bg-gray">&nbsp;</div>
				@endif
				<a href="#" class="btn btn-flat small-box-footer bg-info btn-modal" data-href="{{ action('SellController@show', [$order->id])}}" data-container=".view_modal">@lang('restaurant.order_details') <i class="fa fa-arrow-circle-right"></i></a>

				<a href="#" data-href="{{route('print.checkerPrint', [$order->id, $order->location_id])}}" class="btn btn-flat small-box-footer bg-success print-checker-link" id="print-checker-link">
					<i class="fa fa-print text-muted" aria-hidden="true" title="Print Checker">Print Checker</i>
				</a>
			</div>
		</div>
		@if($loop->iteration % 4 == 0)
			<div class="hidden-xs">
				<div class="clearfix"></div>
			</div>
		@endif
		@if($loop->iteration % 2 == 0)
			<div class="visible-xs">
				<div class="clearfix"></div>
			</div>
		@endif
	@empty
	<div class="col-md-12">
		<h4 class="text-center">@lang('restaurant.no_orders_found')</h4>
	</div>
	@endforelse