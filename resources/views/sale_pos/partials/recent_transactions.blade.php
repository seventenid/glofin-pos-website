@php
	$subtype = '';
@endphp
@if(!empty($transaction_sub_type))
	@php
		$subtype = '?sub_type='.$transaction_sub_type;
	@endphp
@endif

@if(!empty($transactions))
	<table class="table table-slim no-border">
		@foreach ($transactions as $transaction)
			<tr class="cursor-pointer" 
	    		title="Customer: {{optional($transaction->contact)->name}} 
		    		@if(!empty($transaction->contact->mobile) && $transaction->contact->is_default == 0)
		    			<br/>Mobile: {{$transaction->contact->mobile}}
		    		@endif
	    		" >
				<td>
					{{ $loop->iteration}}.
				</td>
				<td>
					{{ $transaction->invoice_no }} ({{optional($transaction->contact)->name}})
					@if(!empty($transaction->table))
						- {{$transaction->table->name}}
					@endif
				</td>
				<td class="display_currency">
					{{ $transaction->final_total }}
				</td>
				<td>
					<a href="{{action('SellPosController@destroy', [$transaction->id])}}" class="delete-sale" style="padding-left: 20px; padding-right: 20px">
						<i class="fa fa-trash text-danger" title="{{__('lang_v1.click_to_delete')}}"></i>
					</a>
				</td>
			</tr>
			<tr class="cursor-pointer">
				<td colspan="3">
					<a class="btn btn-success" href="{{action('SellPosController@edit', [$transaction->id]).$subtype}}">
	    				<i class="fas fa-pen text-white" aria-hidden="true" title="{{__('lang_v1.click_to_edit')}}"> Edit </i>
	    			</a>

	    			<!--<a class="btn btn-info" href="{{action('SellPosController@printInvoice', [$transaction->id])}}" class="print-invoice-link">
	    				<i class="fa fa-print text-white" aria-hidden="true" title="{{__('lang_v1.click_to_print')}}"> Print Invoice </i>
	    			</a>-->

					<a class="btn btn-info" data-href="{{route('print.checkerPrint', [$transaction->id, $transaction->location_id])}}" class="print-checker-link" id="print-checker-link">
						<i class="fa fa-print text-white" aria-hidden="true" title="{{__('lang_v1.click_to_print')}}"> Print Checker </i>
					</a>

					<a class="btn btn-primary" data-href="{{route('print.invoicePrint', [$transaction->id, $transaction->location_id])}}" class="print-invoice-link" id="print-invoice-link">
	    				<i class="fa fa-print text-white" aria-hidden="true" title="{{__('lang_v1.click_to_print')}}"> Print Invoice </i>
	    			</a>
				</td>
			</tr>

		@endforeach
	</table>
@else
	<p>@lang('sale.no_recent_transactions')</p>
@endif