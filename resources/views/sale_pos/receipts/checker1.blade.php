<!-- business information here -->
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!-- <link rel="stylesheet" href="style.css"> -->
        <!--<title>Checker-{{$receipt_details->invoice_no}}</title>-->
		<style type="text/css">
			div{
				display:block;
			}
			.sizeclass{
				/*width:370px;*/
				font-size:22px !important;
				word-break: break-word;
			}
			.f-left{
				margin:0 !important;
				float: left;
			}
			.f-right{
				margin:0 !important;
				float: right;
			}
			.text-left{
				float:left;
				text-align:left;
			}
			.text-right{
				float:right;
				text-align:right;
			}
			@media print {
				* {
					font-size: 12px;
					font-family: 'Times New Roman';
					word-break: break-all;
				}

			.headings{
				font-size: 16px !important;
				font-weight: 700;
				text-transform: uppercase;
			}

			.sub-headings{
				font-size: 14px !important;
				font-weight: 700;
			}

			.border-top{
				border-top: 1px solid #242424;
			}
			.border-bottom{
				border-bottom: 1px solid #242424;
			}

			.border-bottom-dotted{
				border-bottom: 1px dotted darkgray;
			}

			td.serial_number, th.serial_number{
				width: 5%;
				max-width: 5%;
			}

			td.description,
			th.description {
				width: 35%;
				max-width: 35%;
			}

			td.quantity,
			th.quantity {
				width: 15%;
				max-width: 15%;
				word-break: break-all;
			}
			td.unit_price, th.unit_price{
				width: 25%;
				max-width: 25%;
				word-break: break-all;
			}

			td.price,
			th.price {
				width: 20%;
				max-width: 20%;
				word-break: break-all;
			}

			.centered {
				text-align: center;
				align-content: center;
			}

			.checker {
				width: 100%;
				max-width: 100%;
			}

			img {
				max-width: inherit;
				width: auto;
			}

				.hidden-print,
				.hidden-print * {
					display: none !important;
				}
			}
			.table-info {
				width: 100%;
			}
			.table-info tr:first-child td, .table-info tr:first-child th {
				padding-top: 8px;
			}
			.table-info th {
				text-align: left;
			}
			.table-info td {
				text-align: right;
			}
			.logo {
				float: left;
				width:35%;
				padding: 10px;
			}

			.text-with-image {
				float: left;
				width:65%;
			}
			.text-box {
				width: 100%;
				height: auto;
			}
			.m-0 {
				margin:0;
			}
			.textbox-info {
				clear: both;
			}
			.textbox-info p {
				margin-bottom: 0px
			}
			.flex-box {
				display: flex;
				width: 100%;
			}
			.flex-box p {
				width: 50%;
				margin: 0px;
				white-space: nowrap;
			}

			.table-f-12 th, .table-f-12 td {
				font-size: 20px;
				word-break: break-word;
			}

			.bw {
				word-break: break-word;
			}
			.bb-lg {
				border-bottom: 1px solid lightgray;
			}
			.width-100 {
				width: 100% !important;
			}
			.mb-10 {
				margin-bottom: 10px;
			}
			table {
				display: table;
				border-collapse: separate;
				box-sizing: border-box;
				text-indent: initial;
				border-spacing: 2px;
				border-color: grey;
			}
		</style>
    </head>
    <body>
		
        <div class="checker"  style="border-bottom-style: dotted;">
        	<div class="text-box">
            <p class="centered">
            	<!-- Header text -->
				<span class="headings">Checker</span>

				@if(!empty($receipt_details->contact))
					<br/>{!! $receipt_details->contact !!}
				@endif	
			</p>
			</div>
			<div class="border-top textbox-info">
				<p class="f-left"><strong>
					{!! $receipt_details->invoice_no_prefix !!}
				</strong></p>
				<p class="f-right">
					{{$receipt_details->invoice_no}}
				</p>
			</div>
			<div class="textbox-info">
				<p class="f-left"><strong>
					{!! $receipt_details->date_label !!}
				</strong></p>
				<p class="f-right"><strong>
					{{$receipt_details->invoice_date}}<strong>
				</p>
			</div>

        	<!-- Kasir info -->
			@if(!empty($receipt_details->sales_person_label))
				<div class="textbox-info">
					<p class="f-left"><strong>
						{{$receipt_details->sales_person_label}}
					</strong></p>
					<p class="f-right">
						{{$receipt_details->sales_person}}
					</p>
				</div>
			@endif

        	<!-- Waiter info -->
			@if(!empty($receipt_details->service_staff_label) || !empty($receipt_details->service_staff))
	        	<div class="textbox-info">
	        		<p class="f-left"><strong>
	        			{!! $receipt_details->service_staff_label !!}
	        		</strong></p>
	        		<p class="f-right">
	        			{{$receipt_details->service_staff}}
					</p>
	        	</div>
	        @endif

			<!-- Table info -->
	        @if(!empty($receipt_details->table_label) || !empty($receipt_details->table))
	        	<div class="textbox-info">
	        		<p class="f-left"><strong>
	        			@if(!empty($receipt_details->table_label))
							<b>{!! $receipt_details->table_label !!}</b>
						@endif
	        		</strong></p>
	        		<p class="f-right">
	        			{{$receipt_details->table}}
	        		</p>
	        	</div>
	        @endif

	        <!-- customer info -->
			@if(!empty($receipt_details->customer_info))
			<div class="textbox-info">
				<p class="f-left"><strong>
					{{$receipt_details->customer_label}}
				</strong></p>
				<p class="f-right">
					{{$receipt_details->customer_name}}
				</p>
			</div>
			@endif
			
			@if(!empty($receipt_details->client_id_label))
				<div class="textbox-info">
					<p class="f-left"><strong>
						{{ $receipt_details->client_id_label }}
					</strong></p>
					<p class="f-right">
						{{ $receipt_details->client_id }}
					</p>
				</div>
			@endif
			
			@if(!empty($receipt_details->customer_tax_label))
				<div class="textbox-info">
					<p class="f-left"><strong>
						{{ $receipt_details->customer_tax_label }}
					</strong></p>
					<p class="f-right">
						{{ $receipt_details->customer_tax_number }}
					</p>
				</div>
			@endif

			@if(!empty($receipt_details->customer_custom_fields))
				<div class="textbox-info">
					<p class="centered">
						{!! $receipt_details->customer_custom_fields !!}
					</p>
				</div>
			@endif
			
			@if(!empty($receipt_details->customer_rp_label))
				<div class="textbox-info">
					<p class="f-left"><strong>
						{{ $receipt_details->customer_rp_label }}
					</strong></p>
					<p class="f-right">
						{{ $receipt_details->customer_total_rp }}
					</p>
				</div>
			@endif

	        <!-- Shipping info -->
			@if(!empty($receipt_details->shipping_custom_field_1_label))
				<div class="textbox-info">
					<p class="f-left"><strong>
						{!!$receipt_details->shipping_custom_field_1_label!!} 
					</strong></p>
					<p class="f-right">
						{!!$receipt_details->shipping_custom_field_1_value ?? ''!!}
					</p>
				</div>
			@endif
			@if(!empty($receipt_details->shipping_custom_field_2_label))
				<div class="textbox-info">
					<p class="f-left"><strong>
						{!!$receipt_details->shipping_custom_field_2_label!!} 
					</strong></p>
					<p class="f-right">
						{!!$receipt_details->shipping_custom_field_2_value ?? ''!!}
					</p>
				</div>
			@endif
			@if(!empty($receipt_details->shipping_custom_field_3_label))
				<div class="textbox-info">
					<p class="f-left"><strong>
						{!!$receipt_details->shipping_custom_field_3_label!!} 
					</strong></p>
					<p class="f-right">
						{!!$receipt_details->shipping_custom_field_3_value ?? ''!!}
					</p>
				</div>
			@endif
			@if(!empty($receipt_details->shipping_custom_field_4_label))
				<div class="textbox-info">
					<p class="f-left"><strong>
						{!!$receipt_details->shipping_custom_field_4_label!!} 
					</strong></p>
					<p class="f-right">
						{!!$receipt_details->shipping_custom_field_4_value ?? ''!!}
					</p>
				</div>
			@endif
			@if(!empty($receipt_details->shipping_custom_field_5_label))
				<div class="textbox-info">
					<p class="f-left"><strong>
						{!!$receipt_details->shipping_custom_field_5_label!!} 
					</strong></p>
					<p class="f-right">
						{!!$receipt_details->shipping_custom_field_5_value ?? ''!!}
					</p>
				</div>
			@endif
			
	        <!-- ITEM BELI info -->
            <table style="padding-top: 5px !important; border-top:2px solid #000" class="border-bottom width-100 table-f-12 mb-10">
                <tbody>
                	@forelse($receipt_details->lines as $line)
	                    <tr class="bb-lg">
	                        <td class="description">
	                        	<div style="display:flex; width: 100%;">
	                        		<p class="text-left m-0 mt-5 pull-left"><!-- #{{$loop->iteration}}. {{$line['name']}} -->
			                        	@if(!empty($line['product_custom_fields'])), {{$line['product_custom_fields']}} @endif
										
										<!-- Lot number & product expired -->
			                        	@if(!empty($line['lot_number']))<br> {{$line['lot_number_label']}}:  {{$line['lot_number']}} @endif 
			                        	@if(!empty($line['product_expiry'])), {{$line['product_expiry_label']}}:  {{$line['product_expiry']}} @endif

										<!-- Variation-->
			                        	@if(!empty($line['variation']))
			                        		,
			                        		{{$line['product_variation']}} {{$line['variation']}}
			                        	@endif

										<!-- extra, warranty -->
			                        	@if(!empty($line['warranty_name']))
			                            	, 
			                            	<small>
			                            		{{$line['warranty_name']}}
			                            	</small>
			                            @endif
			                            @if(!empty($line['warranty_exp_date']))
			                            	<small>
			                            		- {{@format_date($line['warranty_exp_date'])}}
			                            	</small>
			                            @endif
			                            @if(!empty($line['warranty_description']))
			                            	<small> {{$line['warranty_description'] ?? ''}}</small>
			                            @endif

										<!-- Sell Line Note -->
			                        	@if(!empty($line['sell_line_note']))
			                        	<br>
	                        			<span class="f-8">
			                        		Note: {{$line['sell_line_note']}}
			                        	</span>
			                        	@endif 
	                        		</p>
	                        	</div>
	                        	<div style="display:flex; width: 100%;">
	                        		<p class="text-left width-60 quantity m-0 bw">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	                        			Total : {{$line['quantity']}} 
	                        		</p>
	                        	</div>
	                        </td>
	                    </tr>
	                    @if(!empty($line['modifiers']))
							@foreach($line['modifiers'] as $modifier)
								<tr>
									<td>
										<div style="display:flex;">
	                        				<p style="width: 28px;" class="m-0">
	                        				</p>
	                        				<p class="text-left width-60 m-0" style="margin:0;">
	                        					{{$modifier['name']}} 
			                            		@if(!empty($modifier['sell_line_note']))({{$modifier['sell_line_note']}}) @endif
												<br>
	                        					{{$modifier['quantity']}}
	                        				</p>
	                        				<p class="text-right width-40 m-0">
	                        					{{$modifier['variation']}}
	                        				</p>
	                        			</div>	
			                        </td>
			                    </tr>
							@endforeach
						@endif
                    @endforeach
                </tbody>
            </table>
            @if(!empty($receipt_details->total_quantity_label))
				<div class="flex-box">
					<p class="left text-left">
						{!! $receipt_details->total_quantity_label !!}
					</p>
					<p class="width-50 text-right">
						{{$receipt_details->total_quantity}}
					</p>
				</div>
			@endif

            @if(!empty($receipt_details->additional_notes))
	            <p class="centered" >
	            	{!! nl2br($receipt_details->additional_notes) !!}
	            </p>
            @endif

			@if(!empty($receipt_details->footer_text))
				<p class="centered">
					{!! $receipt_details->footer_text !!}
				</p>
			@endif
        </div>

        <div class="checker"  style="border-bottom-style: dotted;">
		</div>

		<div class="checker"  style="border-bottom-style: dotted;">
			<p class="centered" style="text-align:center">
				Powered by ©GlofinPOS
				<br/>
				Global Informa
			</p>
		</div>

    </body>
</html>

