<!-- business information here -->
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!-- <link rel="stylesheet" href="style.css"> -->
        <!--<title>Receipt-{{$receipt_details->invoice_no}}</title>-->
		<link rel="stylesheet" href="{{ asset('css/vendor.css?v='.$asset_v) }}">
		<link rel="stylesheet" href="{{ asset('css/app.css?v='.$asset_v) }}">
    </head>
    <body style="width:500px">
		<div class="row">

			<!-- Header text -->
			@if(!empty($receipt_details->header_text))
				<div class="col-xs-12">
					{!! $receipt_details->header_text !!}
				</div>
			@endif

			<!-- business information here -->
			<div class="col-xs-5">
				<p style="width: 100% !important" class="word-wrap">
					<span class="pull-left text-left word-wrap">
						<!-- Shop & Location Name  -->
						@if(!empty($receipt_details->display_name))
							<strong>{{$receipt_details->display_name}}</strong>
							<br/>
						@endif

						<!-- Address -->
						@if(!empty($receipt_details->address))
							<small>
							{!! $receipt_details->address !!}
							</small>
						@endif
						
						@if(!empty($receipt_details->contact))
							<br/>{!! $receipt_details->contact !!}
						@endif	
						@if(!empty($receipt_details->contact) && !empty($receipt_details->website))
							, 
						@endif
						@if(!empty($receipt_details->website))
							{{ $receipt_details->website }}
						@endif
						@if(!empty($receipt_details->location_custom_fields))
							<br>{{ $receipt_details->location_custom_fields }}
						@endif

						@if(!empty($receipt_details->sub_heading_line1))
							{{ $receipt_details->sub_heading_line1 }}
						@endif
						@if(!empty($receipt_details->sub_heading_line2))
							<br>{{ $receipt_details->sub_heading_line2 }}
						@endif
						@if(!empty($receipt_details->sub_heading_line3))
							<br>{{ $receipt_details->sub_heading_line3 }}
						@endif
						@if(!empty($receipt_details->sub_heading_line4))
							<br>{{ $receipt_details->sub_heading_line4 }}
						@endif		
						@if(!empty($receipt_details->sub_heading_line5))
							<br>{{ $receipt_details->sub_heading_line5 }}
						@endif

						@if(!empty($receipt_details->tax_info1))
							<b>{{ $receipt_details->tax_label1 }}</b> {{ $receipt_details->tax_info1 }}
						@endif

						@if(!empty($receipt_details->tax_info2))
							<b>{{ $receipt_details->tax_label2 }}</b> {{ $receipt_details->tax_info2 }}
						@endif

						<br>
						<br>
						<!-- Invoice  number, Date  -->
						@if(!empty($receipt_details->invoice_no_prefix))
							<b>{!! $receipt_details->invoice_no_prefix !!}</b>
							{{$receipt_details->invoice_no}}
						@endif

						<br>
						<!-- faktur pajak  -->
						<b>Faktur Pajak</b>
						{{$receipt_details->custom_field_1}}

						@if(!empty($receipt_details->types_of_service))
							<br/>
							<span class="pull-left text-left">
								<strong>{!! $receipt_details->types_of_service_label !!}:</strong>
								{{$receipt_details->types_of_service}}
								<!-- Waiter info -->
								@if(!empty($receipt_details->types_of_service_custom_fields))
									@foreach($receipt_details->types_of_service_custom_fields as $key => $value)
										<br><strong>{{$key}}: </strong> {{$value}}
									@endforeach
								@endif
							</span>
						@endif

						<!-- Table information-->
						@if(!empty($receipt_details->table_label) || !empty($receipt_details->table))
							<br/>
							<span class="pull-left text-left">
								@if(!empty($receipt_details->table_label))
									<b>{!! $receipt_details->table_label !!}</b>
								@endif
								{{$receipt_details->table}}

								<!-- Waiter info -->
							</span>
						@endif

						@if(!empty($receipt_details->sales_person_label))
							<br/>
							<b>{{ $receipt_details->sales_person_label }}</b> 
							
							@if(!empty($receipt_details->commission_agent))
								{{ $receipt_details->commission_agent }}
							@else
								-
							@endif
						@endif
						
					</span>
				</p>
			</div>

			<div class="col-xs-2">
				<p style="width: 100% !important" class="word-wrap">
					<br><br><br>
					<!-- Title of receipt -->
					@if(!empty($receipt_details->invoice_heading))
						<h4 style="text-align:center; margin:auto">
							<strong>{!! $receipt_details->invoice_heading !!}</strong>
						</h4>
					@endif
				</p>
			</div>

			<div class="col-xs-5">
				<p style="width: 100% !important" class="word-wrap">
					<span class="pull-right text-left">
						<!-- customer info -->
						@if(!empty($receipt_details->customer_info))
							<b>{{ $receipt_details->customer_label }}</b> 
							{!! $receipt_details->customer_info !!}
							<br/>
						@endif
						@if(!empty($receipt_details->client_id_label))
							<br/>
							<b>{{ $receipt_details->client_id_label }}</b> {{ $receipt_details->client_id }}
						@endif
						@if(!empty($receipt_details->customer_tax_label))
							<br/>
							<b>{{ $receipt_details->customer_tax_label }}</b> {{ $receipt_details->customer_tax_number }}
						@endif
						@if(!empty($receipt_details->customer_custom_fields))
							<br/>{!! $receipt_details->customer_custom_fields !!}
						@endif
						@if(!empty($receipt_details->customer_rp_label))
							<br/>
							<strong>{{ $receipt_details->customer_rp_label }}</strong> {{ $receipt_details->customer_total_rp }}
						@endif
						
						<b>{{$receipt_details->date_label}}</b> {{$receipt_details->invoice_date}}

						@if(!empty($receipt_details->due_date_label))
						<br><b>{{$receipt_details->due_date_label}}</b> {{$receipt_details->due_date ?? ''}}
						@endif

						@if(!empty($receipt_details->brand_label) || !empty($receipt_details->repair_brand))
							<br>
							@if(!empty($receipt_details->brand_label))
								<b>{!! $receipt_details->brand_label !!}</b>
							@endif
							{{$receipt_details->repair_brand}}
						@endif


						@if(!empty($receipt_details->device_label) || !empty($receipt_details->repair_device))
							<br>
							@if(!empty($receipt_details->device_label))
								<b>{!! $receipt_details->device_label !!}</b>
							@endif
							{{$receipt_details->repair_device}}
						@endif

						@if(!empty($receipt_details->model_no_label) || !empty($receipt_details->repair_model_no))
							<br>
							@if(!empty($receipt_details->model_no_label))
								<b>{!! $receipt_details->model_no_label !!}</b>
							@endif
							{{$receipt_details->repair_model_no}}
						@endif

						@if(!empty($receipt_details->serial_no_label) || !empty($receipt_details->repair_serial_no))
							<br>
							@if(!empty($receipt_details->serial_no_label))
								<b>{!! $receipt_details->serial_no_label !!}</b>
							@endif
							{{$receipt_details->repair_serial_no}}<br>
						@endif
						@if(!empty($receipt_details->repair_status_label) || !empty($receipt_details->repair_status))
							@if(!empty($receipt_details->repair_status_label))
								<b>{!! $receipt_details->repair_status_label !!}</b>
							@endif
							{{$receipt_details->repair_status}}<br>
						@endif
						
						@if(!empty($receipt_details->repair_warranty_label) || !empty($receipt_details->repair_warranty))
							@if(!empty($receipt_details->repair_warranty_label))
								<b>{!! $receipt_details->repair_warranty_label !!}</b>
							@endif
							{{$receipt_details->repair_warranty}}
							<br>
						@endif
						
						<!-- Waiter info -->
						@if(!empty($receipt_details->service_staff_label) || !empty($receipt_details->service_staff))
							<br/>
							@if(!empty($receipt_details->service_staff_label))
								<b>{!! $receipt_details->service_staff_label !!}</b>
							@endif
							{{$receipt_details->service_staff}}
						@endif
						@if(!empty($receipt_details->shipping_custom_field_1_label))
							<br><strong>{!!$receipt_details->shipping_custom_field_1_label!!} :</strong> {!!$receipt_details->shipping_custom_field_1_value ?? ''!!}
						@endif

						@if(!empty($receipt_details->shipping_custom_field_2_label))
							<br><strong>{!!$receipt_details->shipping_custom_field_2_label!!}:</strong> {!!$receipt_details->shipping_custom_field_2_value ?? ''!!}
						@endif

						@if(!empty($receipt_details->shipping_custom_field_3_label))
							<br><strong>{!!$receipt_details->shipping_custom_field_3_label!!}:</strong> {!!$receipt_details->shipping_custom_field_3_value ?? ''!!}
						@endif

						@if(!empty($receipt_details->shipping_custom_field_4_label))
							<br><strong>{!!$receipt_details->shipping_custom_field_4_label!!}:</strong> {!!$receipt_details->shipping_custom_field_4_value ?? ''!!}
						@endif

						@if(!empty($receipt_details->shipping_custom_field_5_label))
							<br><strong>{!!$receipt_details->shipping_custom_field_2_label!!}:</strong> {!!$receipt_details->shipping_custom_field_5_value ?? ''!!}
						@endif
						{{-- sale order --}}
						@if(!empty($receipt_details->sale_orders_invoice_no))
							<br>
							<strong>@lang('restaurant.order_no'):</strong> {!!$receipt_details->sale_orders_invoice_no ?? ''!!}
						@endif

						@if(!empty($receipt_details->sale_orders_invoice_date))
							<br>
							<strong>@lang('lang_v1.order_dates'):</strong> {!!$receipt_details->sale_orders_invoice_date ?? ''!!}
						@endif
					</span>
				</p>
			</div>

		</div>

		<div class="row">
			@includeIf('sale_pos.receipts.partial.common_repair_invoice')
		</div>

		<div class="row">
			<div class="col-xs-12">
				<br/>
				@php
					$p_width = 40;
					$i=1;
				@endphp
				@if(!empty($receipt_details->item_discount_label))
					@php
						$p_width -= 15;
					@endphp
				@endif
				<table style="table table table-responsive border-bottom:1px solid #000">
					<tbody style="border-top:1px solid #000; border-bottom:1px solid #000">
						<tr style="border-top:1px solid #000; border-bottom:1px solid #000">
							<th width="5%">No</th>
							<th width="40%">{{$receipt_details->table_product_label}}</th>
							<th width="15%">Batch / Expired</th>
							<th class="text-right" width="10%">{{$receipt_details->table_qty_label}}</th>
							<th class="text-right" width="10%">{{$receipt_details->table_unit_price_label}}</th>
							<th class="text-right" width="15%" style="padding-right:10px">{{$receipt_details->item_discount_label}}</th>
							<th class="text-right" width="15%">{{$receipt_details->table_subtotal_label}}</th>
						</tr>
						@forelse($receipt_details->lines as $line)
							<tr>
								<td>
									{{$i}}
								</td>
								<td>
									{{$line['name']}} {{$line['product_variation']}} {{$line['variation']}} 

									@if(!empty($line['warranty_name'])) <br><small>{{$line['warranty_name']}} </small>@endif @if(!empty($line['warranty_exp_date'])) <small>- {{@format_date($line['warranty_exp_date'])}} </small>@endif
									@if(!empty($line['warranty_description'])) <small> {{$line['warranty_description'] ?? ''}}</small>@endif
								</td>
								<td>
									@if(isset($line['sell_line_note'])) {{$line['sell_line_note']}} @endif 
									@if(!empty($line['lot_number']))<br> {{$line['lot_number_label']}}:  {{$line['lot_number']}} @endif 
									@if(!empty($line['product_expiry'])), {{$line['product_expiry_label']}}:  {{$line['product_expiry']}} @endif
								</td>
								<td class="text-right">{{$line['quantity']}} {{$line['units']}} </td>
								<td class="text-right">{{$line['unit_price_before_discount']}}</td>
								<td class="text-right" style="padding-right:10px">
									{{$line['line_discount'] ?? '0.00'}}
								</td>
								<td class="text-right">{{$line['line_total']}}</td>
							</tr>
							@if(!empty($line['modifiers']))
								@foreach($line['modifiers'] as $modifier)
									<tr>
										<td>
											{{$modifier['name']}} {{$modifier['variation']}} 
											@if(!empty($modifier['sub_sku'])), {{$modifier['sub_sku']}} @endif @if(!empty($modifier['cat_code'])), {{$modifier['cat_code']}}@endif
											@if(!empty($modifier['sell_line_note']))({{$modifier['sell_line_note']}}) @endif 
										</td>
										<td class="text-right">{{$modifier['quantity']}} {{$modifier['units']}} </td>
										<td class="text-right">{{$modifier['unit_price_inc_tax']}}</td>
										@if(!empty($receipt_details->item_discount_label))
											<td class="text-right">0.00</td>
										@endif
										<td class="text-right">{{$modifier['line_total']}}</td>
									</tr>
								@endforeach
							@endif
							
							@php
								$i+=1;
							@endphp
						@empty
							<tr>
								<td colspan="4">&nbsp;</td>
							</tr>
						@endforelse
					</tbody>
				</table>
				<br>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-6">

				<table class="table table-slim">
					<tr>
						<th class="text-center" style="border:none">
							Penerima
						</th>
						<th class="text-center" style="border:none">
							Hormat Kami
						</th>
					</tr>

					<tr>
						<th class="text-center" style="border:none">
							<br>
						</th>
						<th class="text-center" style="border:none">
							<br>
						</th>
					</tr>

					<tr>
						<th class="text-center" style="border:none">
							(........................)
						</th>
						<td class="text-center" style="border:none">
						@if(!empty($receipt_details->footer_text))
							{!! $receipt_details->footer_text !!}
						@endif
						</td>
					</tr>
				</table>
			</div>

			<div class="col-xs-6">
				<div class="table-responsive">
					<table class="table table-slim">
						<tbody>
							@if(!empty($receipt_details->total_quantity_label))
								<tr class="color-555">
									<th style="width:40%">
										{!! $receipt_details->total_quantity_label !!}
									</th>
									<td class="text-right">
										{{$receipt_details->total_quantity}}
									</td>
								</tr>
							@endif
							<tr>
								<th style="width:40%">
									{!! $receipt_details->subtotal_label !!}
								</th>
								<td class="text-right">
									{{$receipt_details->subtotal}}
								</td>
							</tr>
							@if(!empty($receipt_details->total_exempt_uf))
							<tr>
								<th style="width:40%">
									@lang('lang_v1.exempt')
								</th>
								<td class="text-right">
									{{$receipt_details->total_exempt}}
								</td>
							</tr>
							@endif
							<!-- Shipping Charges -->
							@if(!empty($receipt_details->shipping_charges))
								<tr>
									<th style="width:40%">
										{!! $receipt_details->shipping_charges_label !!}
									</th>
									<td class="text-right">
										{{$receipt_details->shipping_charges}}
									</td>
								</tr>
							@endif

							@if(!empty($receipt_details->packing_charge))
								<tr>
									<th style="width:40%">
										{!! $receipt_details->packing_charge_label !!}
									</th>
									<td class="text-right">
										{{$receipt_details->packing_charge}}
									</td>
								</tr>
							@endif

							<!-- Discount -->
							@if( !empty($receipt_details->discount) )
								<tr>
									<th>
										{!! $receipt_details->discount_label !!}
									</th>

									<td class="text-right">
										(-) {{$receipt_details->discount}}
									</td>
								</tr>
							@endif

							@if( !empty($receipt_details->reward_point_label) )
								<tr>
									<th>
										{!! $receipt_details->reward_point_label !!}
									</th>

									<td class="text-right">
										(-) {{$receipt_details->reward_point_amount}}
									</td>
								</tr>
							@endif

							<!-- Tax -->
							@if( !empty($receipt_details->tax) )
								<tr>
									<th>
										{!! $receipt_details->tax_label !!}
									</th>
									<td class="text-right">
										(+) {{$receipt_details->tax_plus_roundoff }}
									</td>
								</tr>
							@endif

							<!-- Total -->
							<tr>
								<th>
									{!! $receipt_details->total_label !!}
								</th>
								<td class="text-right">
									{{$receipt_details->total}}
									@if(!empty($receipt_details->total_in_words))
										<br>
										<small>({{$receipt_details->total_in_words}})</small>
									@endif
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="col-xs-12">
				<p>{!! nl2br($receipt_details->additional_notes) !!}</p>
			</div>
		</div>

		@if($receipt_details->show_barcode)
			<div class="row">
				<div class="col-xs-12">
					{{-- Barcode --}}
					<img class="center-block" src="data:image/png;base64,{{DNS1D::getBarcodePNG($receipt_details->invoice_no, 'C128', 2,30,array(39, 48, 54), true)}}">
				</div>
			</div>
		@endif

    </body>
</html>