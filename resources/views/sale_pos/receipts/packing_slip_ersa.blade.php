<!-- business information here -->
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!-- <link rel="stylesheet" href="style.css"> -->
        <!--<title>Receipt-{{$receipt_details->invoice_no}}</title>-->
		<link rel="stylesheet" href="{{ asset('css/vendor.css?v='.$asset_v) }}">
		<link rel="stylesheet" href="{{ asset('css/app.css?v='.$asset_v) }}">
    </head>
    <body style="width:500px">
		<div class="row">

			<!-- business information here -->
			<div class="col-xs-6">
				<p style="width: 100% !important" class="word-wrap">
					<span class="pull-left text-left word-wrap">
						<strong>Surat Jalan</strong>
						<br>

						<br>
						<!-- Invoice  number, Date  -->
						@if(!empty($receipt_details->invoice_no_prefix))
							<b>{!! $receipt_details->invoice_no_prefix !!}</b>
							{{$receipt_details->invoice_no}}
						@endif
						
						<br>
						<b>{{$receipt_details->date_label}}</b> {{$receipt_details->invoice_date}}
						
					</span>
				</p>
			</div>

			<div class="col-xs-6">
				<p style="width: 100% !important" class="word-wrap">
					<span class="pull-right text-left">
						<!-- Shop & Location Name  -->
						@if(!empty($receipt_details->display_name))
							<strong>{{$receipt_details->display_name}}</strong>
							<br>
						@endif

						<!-- customer info -->
						@if(!empty($receipt_details->customer_info))
							<b>{{ $receipt_details->customer_label }}</b> 
							{!! $receipt_details->customer_info !!} <br>
						@endif
					</span>
				</p>
			</div>

		</div>

		<div class="row">
			@includeIf('sale_pos.receipts.partial.common_repair_invoice')
		</div>

		<div class="row">
			<div class="col-xs-12">
				@php
					$p_width = 40;
					$i=1;
				@endphp
				@if(!empty($receipt_details->item_discount_label))
					@php
						$p_width -= 15;
					@endphp
				@endif
				<table style="table table table-responsive border-bottom:1px solid #000">
					<thead>
						<tr style="border-top:1px solid #000">
							<th width="5%">No</th>
							<th width="{{$p_width}}%">{{$receipt_details->table_product_label}}</th>
							<th width="15%">Batch / Expired</th>
							<th class="text-right" width="10%">{{$receipt_details->table_qty_label}}</th>
						</tr>
					</thead>
					<tbody style="border-top:1px solid #000; border-bottom:1px solid #000">
						@forelse($receipt_details->lines as $line)
							<tr>
								<td>
									{{$i}}
								</td>
								<td>
									@if(!empty($line['image']))
										<img src="{{$line['image']}}" alt="Image" width="50" style="float: left; margin-right: 8px;">
									@endif
									{{$line['name']}} {{$line['product_variation']}} {{$line['variation']}} 
									@if(!empty($line['sub_sku'])), {{$line['sub_sku']}} @endif @if(!empty($line['brand'])), {{$line['brand']}} @endif @if(!empty($line['cat_code'])), {{$line['cat_code']}}@endif
									@if(!empty($line['product_custom_fields'])), {{$line['product_custom_fields']}} @endif
									

									@if(!empty($line['warranty_name'])) <br><small>{{$line['warranty_name']}} </small>@endif @if(!empty($line['warranty_exp_date'])) <small>- {{@format_date($line['warranty_exp_date'])}} </small>@endif
									@if(!empty($line['warranty_description'])) <small> {{$line['warranty_description'] ?? ''}}</small>@endif
								</td>
								<td>
									{{$line['sell_line_note']}}
									@if(!empty($line['lot_number']))<br> {{$line['lot_number_label']}}:  {{$line['lot_number']}} @endif 
									@if(!empty($line['product_expiry'])), {{$line['product_expiry_label']}}:  {{$line['product_expiry']}} @endif
								</td>
								<td class="text-right">{{$line['quantity']}} {{$line['units']}} </td>
							</tr>
							
							@php
								$i+=1;
							@endphp
						@empty
						@endforelse
					</tbody>
				</table>
				<br>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">

				<table class="table table-slim">
					<tr>
						<th class="text-center" style="border:none">
							Hormat Kami
						</th>
						<th class="text-center" style="border:none">
							Pengirim
						</th>
						<th class="text-center" style="border:none">
							Penerima
						</th>
					</tr>

					<tr>
						<th class="text-center" style="border:none">
							<br>
						</th>
						<th class="text-center" style="border:none">
							<br>
						</th>
						<th class="text-center" style="border:none">
							<br>
						</th>
					</tr>

					<tr>
						<td class="text-center" style="border:none">
							@if(!empty($receipt_details->footer_text))
								{!! $receipt_details->footer_text !!}
							@endif
						</td>
						<th class="text-center" style="border:none">
							(........................)
						</th>
						<th class="text-center" style="border:none">
							(........................)
						</th>
					</tr>
				</table>
			</div>

			<div class="col-xs-12">
				<p>{!! nl2br($receipt_details->additional_notes) !!}</p>
			</div>
		</div>
    </body>
</html>