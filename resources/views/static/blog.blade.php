@extends('layoutss.main')

@section('content')
    <div class="content_wrapper clearfix">
        <div class="sections_group">
            <div class="entry-content">
                <div class="section mcb-section full-width" style="padding-top:120px;background-image:url(content/app6/images/bg-home.png);background-repeat:no-repeat;background-position:center top">
                    
                </div>   

                {{-- <div class="container mt-4 content-justify-center">
                    <div class="row pt-4 content-justify-center">
                        <div class="col-md-6 pt-4 content-justify-center">
                            <form action="/blog" method="GET">
                                <div class="input-group mb-3 pt-4 content-justify-center">
                                    <input type="text" class="form-control" placeholder="Search" name="search">
                                    <button class="btn btn-danger" type="submit">Search</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div> --}}

                @if ($blog->count() > 0)

                {{-- <div class="container mt-3 pt-3">
                    <div class="row">
                        @foreach ($blog as $blogs)
                            <div class="col-md-4 mb-3 justify-content-center">
                                <div class="card">
                                        <div style="max-height: 450px; overflow: hidden;">
                                            <img src="{{ asset('storage/' . $blogs->image) }}" class="card-img-top" alt="">
                                        </div>
                                    <div class="card-body">
                                        <h5 class="card-title">{{ $blogs->title }}</h5>
                                        <p class="mb-3">
                                            {{ $blogs->excerp }}
                                        </p>
                                        <a href="/dashboard/blog/{{ $blogs->id }}" class="btn btn-primary" style="background-color: #319dcb; border-radius: 5%;padding: 5%; margin-top: 50px;">selengkapnya...</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div> --}}

                    <div class="row">
                        <div class="card" style="width: 18rem; margin: 10%; border-color: #000;" >
                            @foreach ($blog as $blogs)
                                <div class="col-md-4 mb-3 justify-content-center" style="margin-bottom: 20%">
                                    <div class="card" style="width: 18rem;">
                                        <img class="card-img-top" src="{{ asset('storage/' . $blogs->image) }}" alt="Card image cap">
                                        <div class="card-body">
                                            <h5 class="card-title">{{ $blogs->title }}</h5>
                                            <p style="color: grey">{{ $blogs->excerp }}</p>
                                            <p class="card-text"> 
                                                <script language="JavaScript">
                                                    var tanggallengkap = new String();
                                                    var namahari = ("Minggu Senin Selasa Rabu Kamis Jumat Sabtu");
                                                    namahari = namahari.split(" ");
                                                    var namabulan = ("Januari Februari Maret April Mei Juni Juli Agustus September Oktober November Desember");
                                                    namabulan = namabulan.split(" ");
                                                    var tgl = new Date();
                                                    var hari = tgl.getDay();
                                                    var tanggal = tgl.getDate();
                                                    var bulan = tgl.getMonth();
                                                    var tahun = tgl.getFullYear();
                                                    tanggallengkap = namahari[hari] + ", " +tanggal + " " + namabulan[bulan] + " " + tahun;
                                                </script>
                                            
                                                <span id='jam' ></span> | <script language='JavaScript'>document.write(tanggallengkap);</script>  
                                            </p>
                                            <a href="/dashboard/blog/{{ $blogs->id }}" class="btn btn-primary" style="background-color: #319dcb; border-radius: 5%;padding: 5%; margin-top: 50px;">selengkapnya...</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @else
                    <p class="text-center f4">Blog not found</p>
                @endif
                
            </div>
        </div>
    </div>
@endsection
