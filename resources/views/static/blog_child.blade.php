@extends('layoutss.main')

@section('content')
<div class="content_wrapper clearfix">
    <div class="sections_group">
        <div class="entry-content">
            <div class="section mcb-section full-width" style="padding-top:120px;background-image:url(content/app6/images/bg-home.png);background-repeat:no-repeat;background-position:center top">
            </div>   
            <div class="section mcb-section" id="features" style="padding-top:60px">
                <div class="section_wrapper mcb-section-inner">
                    <div class="wrap mcb-wrap one valign-middle clearfix">
                        <div class="mcb-wrap-inner">
                            <!-- <div class="column mcb-column one column_column">
                                <div class="column_attr clearfix align_center">
                                    <h2 style="color: #0c6191;"> </h2>
                                </div>
                            </div> -->
                            <div class="column mcb-column one column_divider">
                                <hr class="no_line" style="margin:0 auto 20px">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section mcb-section equal-height-wrap" id="features-1" style="padding-bottom:40px">
                <div class="section_wrapper mcb-section-inner">
                    <div class="wrap mcb-wrap one-second valign-top clearfix">
                        <div class="mcb-wrap-inner">
                            <div class="column mcb-column one column_image">
                                <div class="animate" data-anim-type="fadeInUp">
                                    <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                                        <div class="image_wrapper"><img class="scale-with-grid" style="width: 400px;" src="{{ asset('storage/' . $blog->image) }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wrap mcb-wrap one-second valign-middle clearfix" style="padding:0 0 0 10%; margin-bottom: 0%;" >
                        <div class="mcb-wrap-inner">
                            <div class="column mcb-column one column_column column-margin-50px">
                                <div class="column_attr clearfix">
                                    <h3 style="color: #0c6191;">{{ $blog->title }}</h3>
                                    <hr class="no_line" style="margin:0 auto 0px">
                                    <p>
                                        {!! $blog->content !!}    
                                    <!-- </p><p>
                                    </p> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content_bawah">
                    <p>
                        Point of sale (POS) sendiri merupakan versi modern dari mesin kasir konvensional/cash register yang biasanya sudah dilengkapi dengan cash drawer.  POS pada umumnya berbentuk mesin, tablet, smarpthone, mesin EDC atau perangkat lainnya yang digunakan untuk transaksi di toko.
                    </p>
                    <!-- <p>
                        Dilengkapi dengan fitur yang lengkap, managemen akses pegawai, metode pembayaran, custom pesanan
                        dan lain sebagainya.Dilengkapi dengan fitur yang lengkap, managemen akses pegawai, metode pembayaran, custom pesanan
                        dan lain sebagainya.Dilengkapi dengan fitur yang lengkap, managemen akses pegawai, metode pembayaran, custom pesanan
                        dan lain sebagainya.Dilengkapi dengan fitur yang lengkap, managemen akses pegawai, metode pembayaran, custom pesanan
                        dan lain sebagainya.Dilengkapi dengan fitur yang lengkap, managemen akses pegawai, metode pembayaran, custom pesanan
                        dan lain sebagainya.Dilengkapi dengan fitur yang lengkap, managemen akses pegawai, metode pembayaran, custom pesanan
                        dan lain sebagainya.
                    </p> -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
