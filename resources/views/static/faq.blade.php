@extends('layoutss.main')

@section('content')
    <div class="content_wrapper clearfix">
    <div class="sections_group">
        <div class="entry-content">
            <div class="section mcb-section full-width" style="padding-top:120px;background-image:url(content/app6/images/bg-home.png);background-repeat:no-repeat;background-position:center top">
                
            </div>
                <strong><h1 style="text-align: center; padding-top: 30px; font-family: 'Roboto', Arial, Tahoma, sans-serif; font-style: initial;">Frequently Asked Question</h1></strong>
                <h5 style="text-align: center;">Apa aja sih yang sering ditanyain Glofriends (Glofin Friends) ke Glomin (Glofin Admin) ?</h5>
                <div class="section_wrapper mcb-section-inner" style="padding-top: 70px;">
                    <div class="wrap mcb-wrap one valign-top clearfix">
                        <div class="mcb-wrap-inner" style="font-family: 'Roboto', Arial, Tahoma, sans-serif;">
                            <div class="column mcb-column one column_faq">
                                <div class="faq" style="font-family: roboto;">
                                    <div class="mfn-acc faq_wrapper open1st">
                                        <div class="question">
                                            <div class="title">
                                                <strong><span class="num">1</span>Apa itu Glofin POS?</strong>
                                            </div>
                                            <div class="answer">
                                               <p>
                                                Glofin POS adalah aplikasi kasir online berbasis cloud yang menyediakan perangkat lengkap bagi pelaku usaha kecil, menengah, maupun skala besar.
                                               </p>
                                               <p>
                                                Inovasi yang mempermudah penjual melayani transaksi melalui sistem GLOFIN POS yang dapat diakses dengan berbagai macam perangkat.
                                               </p>
                                            </div>
                                        </div>
                                        <div class="question">
                                            <div class="title">
                                                <strong><span class="num">2</span>Kenapa sih harus pake Glofin POS?</strong>
                                            </div>
                                            <div class="answer">
                                               <p>
                                                Karena, Glofin POS punya banyak keunggulan yang bukan hanya aplikasi kasir, melainkan terdapat fitur lainnya yang memudahkan Glofriends menjalankan manajemen usaha. 
                                               </p>
                                            </div>
                                        </div>
                                        <div class="question">
                                            <div class="title">
                                                <strong><span class="num">3</span>Jenis usaha apa aja yang cocok untuk GlofinPOS? </strong>
                                            </div>
                                            <div class="answer">
                                                <p>
                                                    Glofin mendukung banyak jenis usaha, khususnya buat Glofirends yang baru memulai bisnis seperti fnb, retail, hingga sektor pariwisata dan bisnis usaha berskala besar lainnya.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="question">
                                            <div class="title">
                                               <strong> <span class="num">4</span>Apa saja yang harus Glofriends sediakan jika ingin berlangganan GlofinPOS?</strong>
                                            </div>
                                            <div class="answer">
                                               <p>
                                                Glofriends bisa menggunakan perangkat mulai dari mobile hingga desktop untuk mengoprasikan sistem GlofinPOS. Selain itu, Glomin juga menawarkan pilihan perangkat untuk mendukung program kasir loh!
                                               </p>
                                            </div>
                                        </div>
                                        <div class="question">
                                            <div class="title">
                                                <strong><span class="num">5</span>Ada program trial nggak buat Glofriends?</strong>
                                            </div>
                                            <div class="answer">
                                                Apa sih yang enggak buat Glofriends? Untuk program Demo, Glomin bakal kasih Glofriends gratis 1 bulan trial dengan unlimited transaksi.
                                            </div>
                                        </div>
                                        <div class="question">
                                            <div class="title">
                                                <strong><span class="num">6</span>Apa Glomin  menjamin kemanan data Glofriends?</strong>
                                            </div>
                                            <div class="answer">
                                                Pastinya iya dong! Semua data dari Glofriends bakalan aman sama Glomin!
                                            </div>
                                        </div>
                                        <div class="question">
                                            <div class="title">
                                                <strong><span class="num">7</span>Fitur dari Glofin POS  bisa scan barcode nggak sih?</strong>
                                            </div>
                                            <div class="answer">
                                                Bisa dong! Fitur dari Glofin POS udah dilengkapi dengan barcode reader.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="column mcb-column one column_divider">
                                <hr class="no_line" style="margin: 0 auto 70px;">
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        
            
        </div>
    </div>
</div>
@endsection