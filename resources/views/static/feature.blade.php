@extends('layoutss.main')

@section('content')
    <div class="content_wrapper clearfix">
        <div class="sections_group">
            <div class="entry-content">
                <div class="section mcb-section full-width" style="padding-top:120px;background-image:url(content/app6/images/bg-home.png);background-repeat:no-repeat;background-position:center top">
                </div>   
                <div class="section mcb-section" id="features" style="padding-top:60px">
                    <div class="section_wrapper mcb-section-inner">
                        <div class="wrap mcb-wrap one valign-middle clearfix">
                            <div class="mcb-wrap-inner">
                                <div class="column mcb-column one column_column">
                                    <div class="column_attr clearfix align_center">
                                        <h2 style="color: #0c6191;">Our features</h2>
                                    </div>
                                </div>
                                <div class="column mcb-column one column_divider">
                                    <hr class="no_line" style="margin:0 auto 20px">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section mcb-section equal-height-wrap" id="features-1" style="padding-bottom:40px">
                    <div class="section_wrapper mcb-section-inner">
                        <div class="wrap mcb-wrap one-second valign-top clearfix">
                            <div class="mcb-wrap-inner">
                                <div class="column mcb-column one column_image">
                                    <div class="animate" data-anim-type="fadeInUp">
                                        <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                                            <div class="image_wrapper"><img class="scale-with-grid" src="content/app6/images/fitur1.png">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wrap mcb-wrap one-second valign-middle clearfix" style="padding:0 0 0 5%; margin-bottom: 0%;" >
                            <div class="mcb-wrap-inner">
                                <div class="column mcb-column one column_column column-margin-20px">
                                    <div class="column_attr clearfix">
                                        <h5 class="app6-heading">POS System</h5>
                                        <h3 style="color: #0c6191;">Aplikasi Kasir Online 
                                            <br> dengan fitur lengkap</h3>
                                        <hr class="no_line" style="margin:0 auto 10px">
                                        <p>
                                            Tampilan POS pada sistem kasir dengan tampilan device yang bisa menyesuaikan perangkat yang digunakan oleh user.
                                        </p><p>
                                            Dilengkapi dengan fitur yang lengkap, managemen akses pegawai, metode pembayaran, custom pesanan
                                            dan lain sebagainya.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section mcb-section equal-height-wrap" id="features-2" style="padding-bottom:40px">
                    <div class="section_wrapper mcb-section-inner">
                        <div class="wrap mcb-wrap one-second valign-middle clearfix" style="padding:0 5% 0 0">
                            <div class="mcb-wrap-inner">
                                <div class="column mcb-column one column_column column-margin-20px">
                                    <div class="column_attr clearfix">
                                        <h5 class="app6-heading">Manufacturing</h5>
                                        <h3 style="color: #0c6191;"> Buat daftar Inventori 
                                        <br> Otomatis & Efisien</h3>
                                        <hr class="no_line" style="margin:0 auto 10px">
                                        <p>
                                            Dilengkapi  manajemen inventori yang memudahkan user mengetahui jumlah detail setiap bahan baku yang dimiliki untuk proses produksi.
                                            
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wrap mcb-wrap one-second valign-middle clearfix">
                            <div class="mcb-wrap-inner">
                                <div class="column mcb-column one column_image">
                                    <div class="animate" data-anim-type="fadeInUp">
                                        <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                                            <div class="image_wrapper"><img class="scale-with-grid" src="content/app6/images/fitur3.png">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section mcb-section equal-height-wrap" id="features-3" style="padding-bottom:40px">
                    <div class="section_wrapper mcb-section-inner">
                        <div class="wrap mcb-wrap one-second valign-top clearfix">
                            <div class="mcb-wrap-inner">
                                <div class="column mcb-column one column_image">
                                    <div class="animate" data-anim-type="fadeInUp">
                                        <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                                            <div class="image_wrapper"><img class="scale-with-grid" src="content/app6/images/fitur4.png">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wrap mcb-wrap one-second valign-middle clearfix" style="padding:0 0 0 5%">
                            <div class="mcb-wrap-inner">
                                <div class="column mcb-column one column_column column-margin-20px">
                                    <div class="column_attr clearfix">
                                        <h5 class="app6-heading">Selling</h5>
                                        <h3 style="color: #0c6191;">Rekap Penjualan 
                                            <br>lebih lengkap</h3>
                                        <hr class="no_line" style="margin:0 auto 10px">
                                        <p>
                                            Memudahkan user menganalisa seluruh penjualan penjualan dan stok di toko, cabang, atau kantor pusat. Serta memudahkan user untuk menambahkan promo diskon dan
                                            sistem pengiriman. 
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section mcb-section equal-height-wrap" id="features-2" style="padding-bottom:40px">
                    <div class="section_wrapper mcb-section-inner">
                        <div class="wrap mcb-wrap one-second valign-middle clearfix" style="padding:0 5% 0 0">
                            <div class="mcb-wrap-inner">
                                <div class="column mcb-column one column_column column-margin-20px">
                                    <div class="column_attr clearfix">
                                        <h5 class="app6-heading">CRM</h5>
                                        <h3 style="color: #0c6191;"> Tingkatkan interaksi 
                                        <br> dengan Pelanggan</h3>
                                        <hr class="no_line" style="margin:0 auto 10px">
                                        <p>
                                            Customer Relationship Management  berfungsi untuk mengumpulkan informasi tentang pelanggan dan menggunakannya untuk meningkatkan interaksi positif pelanggan dengan perusahaan. Pada Fitur CRM Glofin POS, User bisa secara otomatis mengirim ucapan ulang tahun kepada pelanggan yang sudah memasukkan data tanggal lahir.
                                            
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wrap mcb-wrap one-second valign-middle clearfix">
                            <div class="mcb-wrap-inner">
                                <div class="column mcb-column one column_image">
                                    <div class="animate" data-anim-type="fadeInUp">
                                        <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                                            <div class="image_wrapper"><img class="scale-with-grid" src="content/app6/images/fitur2.png">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section mcb-section equal-height-wrap" id="features-3" style="padding-bottom:40px">
                    <div class="section_wrapper mcb-section-inner">
                        <div class="wrap mcb-wrap one-second valign-top clearfix">
                            <div class="mcb-wrap-inner">
                                <div class="column mcb-column one column_image">
                                    <div class="animate" data-anim-type="fadeInUp">
                                        <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                                            <div class="image_wrapper"><img class="scale-with-grid" src="content/app6/images/fitur.png">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wrap mcb-wrap one-second valign-middle clearfix" style="padding:0 0 0 5%">
                            <div class="mcb-wrap-inner">
                                <div class="column mcb-column one column_column column-margin-20px">
                                    <div class="column_attr clearfix">
                                        <h5 class="app6-heading">Annual Report</h5>
                                        <h3 style="color: #0c6191;"> Grafik yang memudahkan 
                                            <br>pembacaan data</h3>
                                        <hr class="no_line" style="margin:0 auto 10px">
                                        <p>
                                            Pada menu utama dalam dashboard terdapat grafik analisa yang berisi laporan penjualan seperti pendapatan harian hingga laporan keuangan tahunan. Serta, menganalisa penjualan dan stok di toko, cabang, atau kantor pusat.
                                        
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section mcb-section equal-height-wrap" id="features-2" style="padding-bottom:40px">
                    <div class="section_wrapper mcb-section-inner">
                        <div class="wrap mcb-wrap one-second valign-middle clearfix" style="padding:0 5% 0 0">
                            <div class="mcb-wrap-inner">
                                <div class="column mcb-column one column_column column-margin-20px">
                                    <div class="column_attr clearfix">
                                        <h5 class="app6-heading">HRM</h5>
                                        <h3 style="color: #0c6191;"> Tingkatkan produktifitas team</h3>
                                        <hr class="no_line" style="margin:0 auto 10px">
                                        <p>
                                            Manajemen data kepegawaian lebih mudah dalam satu sistem. Mulai dari sistem presensi dan ketidak hadiran pegawai. Mengatur tunjangan dan potongan gaji hingga pembayaran Payroll.
                                        
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wrap mcb-wrap one-second valign-middle clearfix">
                            <div class="mcb-wrap-inner">
                                <div class="column mcb-column one column_image">
                                    <div class="animate" data-anim-type="fadeInUp">
                                        <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                                            <div class="image_wrapper"><img class="scale-with-grid" src="content/app6/images/fitur5.png">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection