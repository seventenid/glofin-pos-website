<html>
    <head>
        <title></title>
        <style>
            body{
                background-image: url(images/bg-home.png);
                background-size: cover;
                font-family: Arial, Helvetica, sans-serif;
            }
            .bg-notif{
                background-color: white;
                width: 80%;
                height: auto;
                margin-left: 10%;
                margin-top: 3%;
                border-radius: 10px;
                padding: 20px;
                padding-bottom: 20px;
            }
            h6{
                font-size: 15px;
                text-align: center;
                font-family: Arial, Helvetica, sans-serif;
                margin: 0%;
            }
            h3{
                font-size: 25px;
                text-align: center;
                font-family: Arial, Helvetica, sans-serif;
                margin-bottom: 0%;
            }
            .bgtab1 {
                margin-top: 20px;
                margin-bottom: 70px;
            }
            .bgtab2{
                text-align: center;
            }
            .bgtab2 img{
                width: 70%;
            }
        </style>
    </head>
    <body>
        <div class="bg-notif">
            <div class="bgtab1">
                <h3>Selamat, Registrasi kamu berhasil!</h3>
                <h6>Untuk melanjutkan langganan, silahkan ikuti langkah berikut ini :</h6>
            </div>
            <div class="bgtab2">
                <p>
                    <p>1. Log in ke dashboard <a href="{{"/login"}}">glofinpos.com</a></p>
                    <p> 2. Masukkan username dan password yang telah kamu registrasikan sebelumnya</p>
                    <div>
                        <p> 3. Pada dashboard menu kanan atas, klik tombol POS</p>
                        <img src="images/pos.jpeg" alt="">
                    </div>
                    <br> <div>
                            <p> 4. Klik Subscribe pada kotak merah</p>
                            <img src="images/subcribe.jpeg" alt="">
                        </div>
                    <br> <div>
                            <p> 5. Pilih paket yang kamu inginkan</p>
                            <img src="images/package.jpeg" alt="">
                        </div>
                            <p> 6. Jika kamu memilih paket trial, kamu bisa langsung mengoprasikan toko setelah klil tombol subsribe</p>
                    <div>
                        <p> 7. Lanjutkan ke menu pembayaran jika kamu memilih paket UMKM dan Enterprise</p>
                        <img src="images/pay.jpeg" alt="">
                    </div>
                    <p> 8. Lakukan pembayaran sesuai deskripsi yang tertera</p>
                    <p> 9. Konfirmasikan pembayaranmu di <a href="https://wa.me/6281338770043">Whatsapp</a></p>
                    <p> 10. Terimakasih telah berlangganan, registrasimu akan diproses kurang lebih 1x12 jam.</p>
                </p>
            </div>
        </div>
    </body>
</html>