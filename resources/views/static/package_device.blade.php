@extends('layoutss.main')

@section('content')
    <div class="content_wrapper clearfix">
        <div class="sections_group">
            <div class="entry-content">
                <div class="section mcb-section full-width" style="background-image:url(content/app6/images/bg-home.png);background-repeat:no-repeat;background-position:center top; background-size: cover;">
                        <div class="section mcb-section" id="testimonials">
                            <div class="section_wrapper mcb-section-inner">
                                <div class="wrap mcb-wrap one valign-top clearfix">
                                    <div class="mcb-wrap-inner">
                                        <div class="column mcb-column one column_testimonials" >
                                            <div class="testimonials_slider single-photo" style="padding-top: 200px;">
                                                <h1 style="text-align: center; color: white;">Device Package</h1>
                                                <ul class="testimonials_slider_ul" style="padding-top: 0px;">
                                                    <li>
                                                        <div class="package_slider"><img src="content/app6/images/package_div1.png" class="scale-with-grid wp-post-image">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="package_slider"><img src="content/app6/images/package_div2.png" class="scale-with-grid wp-post-image">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="package_slider"><img src="content/app6/images/package_div3.png" class="scale-with-grid wp-post-image">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="package_slider"><img src="content/app6/images/premium.png" class="scale-with-grid wp-post-image">
                                                        </div>
                                                    </li>
                                                </ul>
                                                <div class="slider_pager slider_pagination"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection