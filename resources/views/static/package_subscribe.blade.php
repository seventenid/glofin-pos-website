@extends('layoutss.main')

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />

<script src="https://kit.fontawesome.com/855b355c73.js" crossorigin="anonymous"></script>

<style>
    * {
    margin: 0;
    padding: 0;
    }

    body{
      background: #2d388b;
    }

    /* .main{
      width: 100%;
      height: 100%;
      display: flex;
      justify-content: center;
      align-items: center;
    } */

    .card{
      background: white;
      /* margin: 40px;
      width: 250px;
      height: 400px;
      display: flex; */
      flex-direction: column;
      justify-content: center;
      /* align-items: center; */
      cursor: pointer;
      transition: all .4s;
      box-shadow: 1px 1px 10px rgb(189, 189, 189);
      position: relative;
      overflow: hidden;
      box-shadow: 0 1rem 3remrgba(0,0,0,.175)!important;
    }

    /* .card::before{
      content: '';
      position: absolute;
      top: 10px;
      left: 0;
      width: 100%;
      height: 35px;
      background: #20C997;
      color: white;
      text-align: center;
      justify-content: center;
      align-items: center;
      font-size: 1.4em;
      z-index: 1;
    } */

    .card::after{
      /* content: 'BASIC'; */
      position: absolute;
      top: 40px;
      left: 0;
      width: 100%;
      height: 40px;
      background: #20C997;
      color: white;
      transform-origin: left;
      line-height: 40px;
      text-align: center;
      z-index: 1;
      font-size: 1.4em;
      transition: all .4s;
    }

    .card:hover::after{
      background: #20C997;
      color: white;
      transform: scaleY(1.3);
    }

    .card:hover{
      transform: scale(1.1);
    }

    .card h2{
      padding-top: 10px;
      margin-top: 20px;
      z-index: 2;
    }

    .card p{
      margin: 10px 0;
    }

    .btn {
      width: 120px;
      height: 35px;
      cursor: pointer;
      background: #FB8C00;
      border: none;
      color: white;
      transition: all .4s;
    }

    .btn:hover{
      background: #FD7E14;
    }
    .hide {
    display: none;
    }
    .myDIV{
        color: red
    }    
    .myDIV:hover + .hide {
    display: block;
    color: #319dcb;
    padding-bottom: 10px;
    }
    .myDIV:hover i{
        visibility: hidden;
    }
    .card-title{
        color: #319dcb;
    }
    .accordion-body{
        letter-spacing: -1px;
    }
    .langganan{
        width: 60%
    }
    .tulisan{
        color: #319dcb;
    }
</style>

@section('content')
    <div class="content_wrapper clearfix">
        <div class="sections_group">
            <div class="entry-content">
                <div class="section mcb-section full-width" style="background-image:url(content/app6/images/bg-home.png);background-repeat:no-repeat;background-position:center top; background-size: cover;">
                        <div class="section mcb-section" id="testimonials">
                            <div class="section_wrapper mcb-section-inner">
                                <div class="wrap mcb-wrap one valign-top clearfix">
                                    <div class="mcb-wrap-inner">
                                        <div class="column mcb-column one column_testimonials" style="padding-top: 200px;">
                                            <div class="testimonials_slider single-photo">
                                                <h1 style="text-align: center; color: white; padding-bottom: 5%;"><strong>Subscribe Package</strong> </h1>
                                                {{-- card pakage --}}
                                                <div class="">
                                                    <div class="row d-flex align-content-between">      
                                                        <div class="col-md-4 col-12 text-center justify-content-center">
                                                            <div class="row">
                                                                <div class="col-md-1"></div>
                                                                <div class="col-md-10 col-12">
                                                                    <div class="card card-box-shadow mb-5 bordered rounded-5" style="width: 100%; height: 1000px; margin: 5px; border-width: 5px; border-color: #fff000">
                                                                     <h4 class="card-title mt-5 " style="color: #319dcb"><strong>TRIAL</strong></h4>
                                                                    <div class="justify-content-center mt-4 mb-4">
                                                                        <img src="{{ asset('content/img/TRIAL.png') }}" style="width:20% ;" class="card-img-top" alt="..." height="50px">
                                                                    </div>
                                                                    <div class="card-body">
                                                                        <table class="table table-hover">
                                                                            <tbody>
                                                                                <div class="row mb-3">
                                                                                    <h5 class="mb-0"><strong>Gratis</strong></h5>
                                                                                    <span class="mt-0">Selama 7 Hari</span>
                                                                                </div><br><br>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan text-start">100 Transaksi</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan text-start">100 Produk Inventori</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan text-start">Report Penjualan</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan text-start">Pencatatan Keuangan</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan text-start">Kasir Online</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan text-start">Manajemen Kontak</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan
                                                                                     text-start">Memo Pemberitahuan</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan text-start">Analisa Bisnis</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan text-start">Manufakturing</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan text-start">Penyesuaian Stok</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan text-start">Module Restaurant</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan text-start">CRM & HRM</td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                    <a href="/business/register"><button type="button" class="button_warning langganan " style="font-size: 15px; ">Coba Gratis</button></a>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-1"></div>
                                                            </div>
                                                        </div>    
                                                        <div class="col-md-4 col-12 text-center justify-content-center">
                                                            <div class="row">
                                                                <div class="col-md-1"></div>
                                                                <div class="col-md-10 col-12">
                                                                    <div class="card card-box-shadow mb-5 bordered rounded-5" style="width: 100%; height: 1000px; margin: 5px; border-width: 5px; border-color: #fff000">
                                                                     <h4 class="card-title mt-5" style="color: #319dcb;"><strong>UMKM</strong></h4>
                                                                    <div class="justify-content-center mt-4 mb-4">
                                                                        <img src="{{ asset('content/img/Layer.png') }}" style="width:20% ;" class="card-img-top" alt="..." height="50px">
                                                                    </div>
                                                                    <div class="card-body">
                                                                        <div class="row mb-3">
                                                                            <h5 class="mb-0"><strong>120rb/bulan</strong></h5>
                                                                            <span class="mt-1">(Minimal Subscribe 1 tahun)</span>
                                                                            <h5>harga normal 150rb/bulan</h5>
                                                                        </div>
                                                                        <table class="table table-hover">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan text-start">Transaksi Unlimited</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan text-start">Unlimited Produk Inventori</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan text-start">Report Penjualan</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan text-start">Pencatatan Keuangan</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan text-start">Kasir Online</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan text-start">Manajemen Kontak</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan text-start">Memo Pemberitahuan</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan text-start">Analisa Bisnis</td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                    <a href="/business/register"><button type="button" class="button_warning langganan " style="font-size: 15px;">Berlangganan</button></a>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-1"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 col-12 text-center justify-content-center">
                                                            <div class="row">
                                                                <div class="col-md-1"></div>
                                                                <div class="col-md-10 col-12">
                                                                    <div class="card card-box-shadow mb-5 bordered rounded-5" style="width: 100%; height: 1000px; margin: 5px; border-width: 5px; border-color: #fff000">
                                                                     <h4 class="card-title mt-5 " style="color: #319dcb"><strong>ENTERPRISE</strong></h4>
                                                                    <div class="justify-content-center mt-4 mb-4">
                                                                        <img src="{{ asset('content/img/enterprise.png') }}" style="width:20% ;" class="card-img-top" alt="..." height="50px">
                                                                    </div>
                                                                    <div class="card-body">
                                                                        <table class="table table-hover">
                                                                            <tbody>
                                                                                <div class="row mb-3">
                                                                                    <h5 class="mb-0"><strong>500rb/bulan</strong></h5>
                                                                                    <span class="mt-0">(Minimal Subscribe 1 tahun)</span>
                                                                                    <h5>harga normal 550rb/bulan</h5>
                                                                                </div>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan text-start">Transaksi Unlimited</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan text-start">Unlimited Produk Inventori</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan text-start">Report Penjualan</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan text-start">Pencatatan Keuangan</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan text-start">Kasir Online</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan text-start">Manajemen Kontak</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan
                                                                                     text-start">Memo Pemberitahuan</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan text-start">Analisa Bisnis</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan text-start">Manufakturing</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan text-start">Penyesuaian Stok</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan text-start">Module Restaurant</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th scope="row" style="width: 5%;" class="tulisan"><i class="fa-solid fa-check"></i></i></th>
                                                                                    <td class="tulisan text-start">CRM & HRM</td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                    <a href="/business/register"><button type="button" class="button_warning langganan " style="font-size: 15px; ">Berlangganan</button></a>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-1"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {{-- end card pakage --}}


                                                <div class="container justify-content-center mt-5">
                                                    <div class="row mt-3 mb-4 justify-content-center">
                                                            <button class="btn rounded-pill justify-content-center" style="width: 30%; height: 40px; position: relative; background-color:#fff000 " type="button" data-bs-toggle="collapse" data-bs-target="#collapseWidthExample" aria-expanded="false" aria-controls="collapseWidthExample">
                                                                <p class="mt-1 tulisan" style="font-size: 15px">Lihat Perbandingan Fitur >></i></p>
                                                            </button>
                                                    </div>
                                                </div>
                                                {{-- table --}}
                                                <div class="container justify-content-center mt-5">
                                                    <div class="row mt-3 mb-4 justify-content-center">
                                                        <h1 style="color: white; text-align: center"><strong>Tambahan Fitur</strong></h1>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 card-shadow-lg border-warning m-2 mb-3 collapse collapse-horizontal" id="collapseWidthExample">
                                                    <table class="table  table-hover bordered table-shadow mb-4" style="background-color: white; color: #319dcb; border-width: 5px;">
                                                        <thead>
                                                            <tr style="background-color: #fff000">
                                                                <td scope="col" width="30%" style="color: #319dcb"><h4 class="mt-4" style="font-size: 20px; color: #319dcb">BENEFIT</h4></td>
                                                                <td scope="col" width="23%" style="color: #319dcb"><h4 class="mt-4" style="font-size: 20px; color: #319dcb">TRIAL</h4></td>
                                                                <td scope="col" width="23%" style="color: #319dcb"><h4 class="mt-4" style="font-size: 20px; color: #319dcb">UMKM</h4></td>
                                                                <td scope="col" width="23%" style="color: #319dcb"><h4 class="mt-4" style="font-size: 20px; color: #319dcb">ENTERPRISE</h4></td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td scope="col" class="tulisan eicon-text-align-left ml-5 pl-5" style="">Training</td>
                                                                <td scope="col" class="tulisan"> 1x</td>
                                                                <td scope="col" class="tulisan"> 3x</td>
                                                                <td scope="col" class="tulisan">Unlimited</td>
                                                            </tr>
                                                            <tr>
                                                                <td scope="col" class="tulisan eicon-text-align-left ml-5 pl-5" style="">Durasi Penyimpanan Data</td>
                                                                <td scope="col" class="tulisan"> 1 bulan</td>
                                                                <td scope="col" class="tulisan">Unlimited</td>
                                                                <td scope="col" class="tulisan">Unlimited</td>
                                                            </tr>
                                                            <tr>
                                                                <td scope="col" class="tulisan eicon-text-align-left ml-5 pl-5" style="">Inventori</td>
                                                                <td scope="col" class="tulisan">100 produk</td>
                                                                <td scope="col" class="tulisan">Unlimited</td>
                                                                <td scope="col" class="tulisan">Unlimited</td>
                                                            </tr>
                                                            <tr>
                                                                <td scope="col" class="tulisan eicon-text-align-left ml-5 pl-5" style="">Multi Outlite</td>
                                                                <td scope="col" class="tulisan">1 Outlet</td>
                                                                <td scope="col" class="tulisan">1 Outlet</td>
                                                                <td scope="col" class="tulisan">2 Outlet</td>
                                                            </tr>
                                                            <tr>
                                                                <td scope="col" class="tulisan eicon-text-align-left ml-5 pl-5" style="">Kasir Online</td>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td scope="col" class="tulisan eicon-text-align-left ml-5 pl-5" style="">Rekap Kas</td>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                            </tr>
                                                            <tr>
                                                                <td scope="col" class="tulisan eicon-text-align-left ml-5 pl-5" style="">Management Kontak</td>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                            </tr>
                                                            <tr>
                                                                <td scope="col" class="tulisan eicon-text-align-left ml-5 pl-5" style="">Manajemen Produksi</td>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                                <th scope="col" class="text-danger"><div class="myDIV"><i class="fa-solid fa-lock"></i></div>
                                                                    <div class="hide">Unlock 199k</div>
                                                                </th>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                            </tr>
                                                            <tr>
                                                                <td scope="col" class="tulisan eicon-text-align-left ml-5 pl-5" style="">Laporan Pembelian</td>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                            </tr>
                                                            <tr>
                                                                <td scope="col" class="tulisan eicon-text-align-left ml-5 pl-5" style="">Laporan penjualan</td>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                            </tr>
                                                            <tr>
                                                                <td scope="col" class="tulisan eicon-text-align-left ml-5 pl-5" style="">Transfer dan Penyesuaian Stok</td>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                                <th scope="col" class="text-danger"><div class="myDIV"><i class="fa-solid fa-lock"></i></div>
                                                                    <div class="hide">Unlock 99k</div>
                                                                </th>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                            </tr>
                                                            <tr>
                                                                <td scope="col" class="tulisan eicon-text-align-left ml-5 pl-5" style="">Pembiayaan</td>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                            </tr>
                                                            <tr>
                                                                <td scope="col" class="ttulisan eicon-text-align-left ml-5 pl-5" style="">Akun Pembayaran</td>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                                <th scope="col" class="text-danger"><div class="myDIV"><i class="fa-solid fa-lock"></i></div>
                                                                    <div class="hide">Unlock 199k</div>
                                                                </th>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                            </tr>
                                                            <tr>
                                                                <td scope="col" class="tulisan eicon-text-align-left ml-5 pl-5" style="">Laporan Penjualan Lengkap</td>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                            </tr>
                                                            <tr>
                                                                <td scope="col" class="tulisan eicon-text-align-left ml-5 pl-5" style="">Management Reservasi</td>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                                <th scope="col" class="text-danger"><div class="myDIV"><i class="fa-solid fa-lock"></i></div>
                                                                    <div class="hide">Unlock 49k</div>
                                                                </th>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                            </tr>
                                                            <tr>
                                                                <td scope="col" class="tulisan eicon-text-align-left ml-5 pl-5" style="">Modul Restoran</td>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                                <th scope="col" class="text-danger"><div class="myDIV"><i class="fa-solid fa-lock"></i></div>
                                                                    <div class="hide">Unlock 199k</div>
                                                                </th>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                            </tr>
                                                            <tr>
                                                                <td scope="col" class="tulisan eicon-text-align-left ml-5 pl-5" style="">CRM & HRM</td>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                                <th scope="col" class="text-danger"><div class="myDIV"><i class="fa-solid fa-lock"></i></div>
                                                                    <div class="hide">Unlock 250k</div>
                                                                </th>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                            </tr>
                                                            <tr>
                                                                <td scope="col" class="tulisan eicon-text-align-left ml-5 pl-5" style="">Template Pemberitahuan</td>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                            </tr>
                                                            <tr>
                                                                <td scope="col" class="tulisan eicon-text-align-left ml-5 pl-5" style="">Management Tugas</td>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                                <th scope="col" class="text-danger"><div class="myDIV"><i class="fa-solid fa-lock"></i></div>
                                                                    <div class="hide">Unlock 49K</div>
                                                                </th>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                            </tr>
                                                            <tr>
                                                                <td scope="col" class="tulisan eicon-text-align-left ml-5 pl-5" style="">Invoice Online</td>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                            </tr>
                                                            <tr>
                                                                <td scope="col" class="tulisan eicon-text-align-left ml-5 pl-5" style="">QR Menu</td>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                            </tr>
                                                            <tr>
                                                                <td scope="col" class="tulisan eicon-text-align-left ml-5 pl-5" style="">Order Online</td>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                                <th scope="col" class="text-danger"><div class="myDIV"><i class="fa-solid fa-lock"></i></div>
                                                                    <div class="hide">Unlock 199K</div>
                                                                </th>
                                                                <th scope="col" class="tulisan"><i class="tulisan fa-solid fa-lock-open"></i></th>
                                                            </tr>
                                                        </tbody>
                                                            <tr>
                                                                <td></td>
                                                                <td>
                                                                    <a href="/business/register"><button type="button" class="btn btn-warning" style="margin-top: 10px; background-color: #319dcb; color: #fff">Coba Gratis</button></a>
                                                                </td>
                                                                <td>
                                                                    <a href="/business/register"><button type="button" class="btn btn-warning" style="margin-top: 10px; background-color: #319dcb; color: #fff">Berlangganan</button></a>
                                                                </td>
                                                                <td>
                                                                    <a href="/business/register"><button type="button" class="btn btn-warning" style="margin-top: 10px; background-color: #319dcb; color: #fff">Berlangganan</button></a>
                                                                </td>
                                                            </tr>
                                                    </table>
                                                </div>
                                                {{-- end table --}}

                                                {{-- card fitur --}}
                                                <div class="container">
                                                    <div class="row justify-content-center mt-3">
                                                        <div class="col-md-4 mb-1 col-12 mt-5">
                                                            <div class="card bordered rounded-5" style="border-width: 0px 0px 5px 0px; border-color: #fff000">
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-md-4 justify-content-end">
                                                                            {{-- <div class="justify-content-end mt-2 mb-2"> --}}
                                                                                <img src="{{ asset('content/img/crmhrm.png') }}" class="card-img-top mt-40%" style="height: 50px; width: 70px;" width="20%" alt="">
                                                                            {{-- </div> --}}
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                            <h5 class="card-title">CRM & HRM</h5>
                                                                            <div style="float: left;">
                                                                                <a href="#" style="color: #319dcb;">250K/bulan</a>
                                                                            </div>
                                                                            <button class="accordion-button collapsed text-secondary border-none" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                                                baca selengkapnya >>
                                                                            </button>
                                                                            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                                                                <div class="accordion-body">
                                                                                    berfungsi untuk manajemen pegawai dan customer relationship
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 mb-1 col-12 mt-5">
                                                            <div class="card bordered rounded-5" style="border-width: 0px 0px 5px 0px; border-color: #fff000">
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-md-4 justify-content-end">
                                                                            {{-- <div class="justify-content-end mt-2 mb-2"> --}}
                                                                                <img src="{{ asset('content/img/outlite.png') }}" class="card-img-top mt-40%" style="height: 60px; width: 60px;" width="20%" alt="">
                                                                            {{-- </div> --}}
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                            <h5 class="card-title">Multi Outlet</h5>
                                                                            <div style="float: left;">
                                                                                <a href="#" style="color: #319dcb;">UMKM 100K <br> Enterprise 200K</a>
                                                                            </div>
                                                                            <button class="accordion-button collapsed text-secondary border-none" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                                                baca selengkapnya >>
                                                                            </button>
                                                                            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                                                                <div class="accordion-body">
                                                                                    Manajemen multi cabang untuk toko yang memiliki lebih dari satu outlet
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 mb-1 col-12 mt-5">
                                                            <div class="card bordered rounded-5" style="border-width: 0px 0px 5px 0px; border-color: #fff000">
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-md-4 justify-content-end">
                                                                            {{-- <div class="justify-content-end mt-2 mb-2"> --}}
                                                                                <img src="{{ asset('content/img/ManajemenAkunBank.png') }}" class="card-img-top mt-40%" style="height: 60px; width: 60px;" width="20%" alt="">
                                                                            {{-- </div> --}}
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                            <h5 class="card-title">Payment Account</h5>
                                                                            <div style="float: left;">
                                                                                <a href="#" style="color: #319dcb;">199K/bulan</a>
                                                                            </div>
                                                                            <button class="accordion-button collapsed text-secondary border-none" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                                                baca selengkapnya >>
                                                                            </button>
                                                                            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                                                                <div class="accordion-body">
                                                                                    pencatatan keuangan ke bank ke akun owner
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 mb-1 col-12 mt-3">
                                                            <div class="card bordered rounded-5" style="border-width: 0px 0px 5px 0px; border-color: #fff000">
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-md-4 justify-content-end">
                                                                            {{-- <div class="justify-content-end mt-2 mb-2"> --}}
                                                                                <img src="{{ asset('content/img/ManjemenReservasi.png') }}" class="card-img-top mt-40%" style="height: 60px; width: 60px;" width="20%" alt="">
                                                                            {{-- </div> --}}
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                            <h5 class="card-title">Booking</h5>
                                                                            <div style="float: left;">
                                                                                <a href="#" style="color: #319dcb;">49K/bulan</a>
                                                                            </div>
                                                                            <button class="accordion-button collapsed text-secondary border-none" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                                                baca selengkapnya >>
                                                                            </button>
                                                                            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                                                                <div class="accordion-body">
                                                                                    Manajemen reservasi venue
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 mb-1 col-12 mt-3">
                                                            <div class="card bordered rounded-5" style="border-width: 0px 0px 5px 0px; border-color: #fff000">
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-md-4 justify-content-end">
                                                                            {{-- <div class="justify-content-end mt-2 mb-2"> --}}
                                                                                <img src="{{ asset('content/img/Manufakturing.png') }}" class="card-img-top mt-40%" style="height: 60px; width: 70px;" width="20%" alt="">
                                                                            {{-- </div> --}}
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                            <h5 class="card-title">Manufacturing</h5>
                                                                            <div style="float: left;">
                                                                                <a href="#" style="color: #319dcb;">199K/bulan</a>
                                                                            </div>
                                                                            <button class="accordion-button collapsed text-secondary border-none" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                                                baca selengkapnya >>
                                                                            </button>
                                                                            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                                                                <div class="accordion-body">
                                                                                    Manajemen produksi yang dimulai dari bahan mentah hingga produk siap jual
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 mb-1 col-12 mt-3">
                                                            <div class="card bordered rounded-5" style="border-width: 0px 0px 5px 0px; border-color: #fff000">
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-md-4 justify-content-end">
                                                                            {{-- <div class="justify-content-end mt-2 mb-2"> --}}
                                                                                <img src="{{ asset('content/img/MemoPegawai.png') }}" class="card-img-top mt-40%" style="height: 70px; width: 60px;" width="20%" alt="">
                                                                            {{-- </div> --}}
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                            <h5 class="card-title">Memo Pegawai</h5>
                                                                            <div style="float: left;">
                                                                                <a href="#" style="color: #319dcb;">49K/bulan</a>
                                                                            </div>
                                                                            <button class="accordion-button collapsed text-secondary border-none" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                                                baca selengkapnya >>
                                                                            </button>
                                                                            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                                                                <div class="accordion-body">
                                                                                    To do list untuk karyawan yang berisi , document, memo, etc
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 mb-5 col-12 mt-3">
                                                            <div class="card bordered rounded-5" style="border-width: 0px 0px 5px 0px; border-color: #fff000">
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-md-4 justify-content-end">
                                                                            {{-- <div class="justify-content-end mt-2 mb-2"> --}}
                                                                                <img src="{{ asset('content/img/ModulRestoran.png') }}" class="card-img-top mt-40%" style="height: 65px; width: 60px;" width="20%" alt="">
                                                                            {{-- </div> --}}
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                            <h5 class="card-title">Restoran Module</h5>
                                                                            <div style="float: left;">
                                                                                <a href="#" style="color: #319dcb;">199K/bulan</a>
                                                                            </div>
                                                                            <button class="accordion-button collapsed text-secondary border-none" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                                                baca selengkapnya >>
                                                                            </button>
                                                                            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                                                                <div class="accordion-body">
                                                                                    Manajemen restaurant dari kitchen, bar, service type, hingga manajemen meja
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 mb-5 col-12 mt-3">
                                                            <div class="card bordered rounded-5" style="border-width: 0px 0px 5px 0px; border-color: #fff000">
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-md-4 justify-content-end">
                                                                            {{-- <div class="justify-content-end mt-2 mb-2"> --}}
                                                                                <img src="{{ asset('content/img/PenyesuaiandanStockTransfer.png') }}" class="card-img-top mt-40%" style="height: 55px; width: 60px;" width="20%" alt="">
                                                                            {{-- </div> --}}
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                            <h5 class="card-title">Stock Adjusment</h5>
                                                                            <div style="float: left;">
                                                                                <a href="#" style="color: #319dcb;">99K/bulan</a>
                                                                            </div>
                                                                            <button class="accordion-button collapsed text-secondary border-none" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                                                baca selengkapnya >>
                                                                            </button>
                                                                            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                                                                <div class="accordion-body">
                                                                                    management dengan stock opname dan perpindahan stock ke cabang
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 mb-5 col-12 mt-3">
                                                            <div class="card bordered rounded-5" style="border-width: 0px 0px 5px 0px; border-color: #fff000">
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-md-4 justify-content-end">
                                                                            {{-- <div class="justify-content-end mt-2 mb-2"> --}}
                                                                                <img src="{{ asset('content/img/qr.png') }}" class="card-img-top mt-40%" style="height: 60px; width: 65px;" width="20%" alt="">
                                                                            {{-- </div> --}}
                                                                        </div>
                                                                        <div class="col-md-8">
                                                                            <h5 class="card-title">Order Online</h5>
                                                                            <div style="float: left;">
                                                                                <a href="#" style="color: #319dcb;">199K/bulan</a>
                                                                            </div>
                                                                            <button class="accordion-button collapsed text-secondary border-none" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                                                baca selengkapnya >>
                                                                            </button>
                                                                            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                                                                <div class="accordion-body">
                                                                                    Menu QR dengan auto order
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
    
                                                    </div>
                                                </div>
                                                {{-- end card fitur --}}

                                                <div class="container justify-content-center mt-5">
                                                    <div class="row mt-5 mb-4 justify-content-center">
                                                        <div class="justify-content-center text-center" style="width: 70%;">
                                                            <h1 class="text-white"><strong>Belum memiliki perangkat?</strong></h1>
                                                            <h5 class="text-white"><span>Glofin memiliki beragam paket perangkat agar bisnismu makin hebat.</span></h5>
                                                            <a href="{{ url('package_device') }}">
                                                                <h5 class="" style="color: yellow">Lihat penawaran paket >></h5>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>
