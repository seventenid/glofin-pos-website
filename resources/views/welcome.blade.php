@extends('layoutss.main')

@section('content')    
    <div class="content_wrapper clearfix">
        <div class="sections_group">
            <div class="entry-content">
                <div class="section mcb-section full-width" style="padding-top:200px;background-image:url(content/app6/images/bg-home.png);background-repeat:no-repeat;background-position:center top">
                    <div class="section_wrapper mcb-section-inner">
                        <div class="wrap mcb-wrap one valign-top clearfix">
                            <div class="mcb-wrap-inner">
                                <div class="column mcb-column one column_column">
                                    <div class="column_attr clearfix align_center">
                                        <strong><h2 style="color: #fff; font-family: 'Roboto', Arial, Tahoma, sans-serif;">Always be your partner</h2></strong>
                                        <hr class="no_line" style="margin:0 auto 10px">
                                        <h4 style="color: #fff;">Optimalkan bisnismu dengan All in One System bersama Glofin POS.</h4>
                                    </div>
                                </div>
                                <div class="column mcb-column one column_divider">
                                    <hr class="no_line" style="margin:0 auto 20px">
                                </div>
                                <div class="column mcb-column one column_button">
                                    <div class="button_align align_center">
                                        <strong><a class="button scroll button_left button_size_2 button_theme button_js" href="/business/register"><span class="button_label" style="font-size: 20px; text-align: center;">Coba Gratis</span></a></strong>
                                    </div>
                                </div>
                                <div class="column mcb-column one column_divider">
                                    <hr class="no_line" style="margin:0 auto 10px">
                                </div>
                            </div>
                        </div>
                        <div class="wrap mcb-wrap one valign-top clearfix">
                            <div class="mcb-wrap-inner">
                                <div class="column mcb-column one column_image">
                                    <div class="animate" data-anim-type="fadeInUp">
                                        <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">

                                            <div class="section mcb-section" id="testimonials">
                                                <div class="section_wrapper mcb-section-inner">
                                                    <div class="wrap mcb-wrap one valign-top clearfix">
                                                        <div class="mcb-wrap-inner">
                                                            <div class="column mcb-column one column_testimonials" >
                                                                <div class="testimonials_slider single-photo" >
                                                                    <div class="slider_pager slider_pagination">
                                                                    <ul class="testimonials_slider_ul" style="transition: all 0.1s;">
                                                                        <li>
                                                                            <div class="package_slider"><img src="content/app6/images/pic1.png" class="scale-with-grid wp-post-image">
                                                                            </div>
                                                                        </li>
                                                                        <!-- <li>
                                                                            <div class="package_slider"><img src="content/app6/images/promo4k.png" class="scale-with-grid wp-post-image">
                                                                            </div>
                                                                        </li> -->
                                                                        <li>
                                                                            <div class="package_slider"><img src="content/app6/images/pic3.png" class="scale-with-grid wp-post-image">
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                    
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="image_wrapper"><img class="scale-with-grid" src="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section mcb-section" id="features" style="padding-top:60px">
                    <div class="section_wrapper mcb-section-inner">
                        <div class="wrap mcb-wrap one valign-middle clearfix">
                            <div class="mcb-wrap-inner">
                                <div class="column mcb-column one column_column">
                                    <div class="column_attr clearfix align_center">
                                        <h2 style="color: #0c6191;">Our Features</h2>
                                    </div>
                                </div>
                                <div class="column mcb-column one column_divider">
                                    <hr class="no_line" style="margin:0 auto 20px">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section mcb-section equal-height-wrap" id="features-1" style="padding-bottom:40px">
                    <div class="section_wrapper mcb-section-inner">
                        <div class="wrap mcb-wrap one-second valign-top clearfix">
                            <div class="mcb-wrap-inner">
                                <div class="column mcb-column one column_image">
                                    <div class="animate" data-anim-type="fadeInUp">
                                        <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                                            <div class="image_wrapper"><img class="scale-with-grid" src="content/app6/images/fitur1.png">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wrap mcb-wrap one-second valign-middle clearfix" style="padding:0 0 0 5%; margin-bottom: 0%;" >
                            <div class="mcb-wrap-inner">
                                <div class="column mcb-column one column_column column-margin-20px">
                                    <div class="column_attr clearfix">
                                        <h5 class="app6-heading">POS System</h5>
                                        <h3 style="color: #0c6191;">Fitur Lengkap</h3>
                                        <hr class="no_line" style="margin:0 auto 10px">
                                        <p>
                                            Tampilan POS pada sistem kasir dengan tampilan device yang bisa menyesuaikan perangkat yang digunakan oleh user.
                                        </p><p>
                                            Dilengkapi dengan fitur yang lengkap, managemen akses pegawai, metode pembayaran, custom pesanan
                                            dan lain sebagainya.
                                            <br>
                                            <br><a href="{{ '/feature' }}" style="color: #319dcb;">Read more...</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section mcb-section equal-height-wrap" id="features-2" style="padding-bottom:40px">
                    <div class="section_wrapper mcb-section-inner">
                        <div class="wrap mcb-wrap one-second valign-middle clearfix" style="padding:0 5% 0 0">
                            <div class="mcb-wrap-inner">
                                <div class="column mcb-column one column_column column-margin-20px">
                                    <div class="column_attr clearfix">
                                        <h5 class="app6-heading">Manufacturing</h5>
                                        <h3 style="color: #0c6191;"> Buat Daftar Inventori 
                                        <br> Otomatis & Efisien</h3>
                                        <hr class="no_line" style="margin:0 auto 10px">
                                        <p>
                                            Dilengkapi  manajemen inventori yang memudahkan user mengetahui jumlah detail setiap bahan baku yang dimiliki untuk proses produksi.
                                            <br>
                                            <br><a href="features.html" style="color: #319dcb;">Read more...</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wrap mcb-wrap one-second valign-middle clearfix">
                            <div class="mcb-wrap-inner">
                                <div class="column mcb-column one column_image">
                                    <div class="animate" data-anim-type="fadeInUp">
                                        <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                                            <div class="image_wrapper"><img class="scale-with-grid" src="content/app6/images/fitur3.png">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section mcb-section equal-height-wrap" id="features-3" style="padding-bottom:40px">
                    <div class="section_wrapper mcb-section-inner">
                        <div class="wrap mcb-wrap one-second valign-top clearfix">
                            <div class="mcb-wrap-inner">
                                <div class="column mcb-column one column_image">
                                    <div class="animate" data-anim-type="fadeInUp">
                                        <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                                            <div class="image_wrapper"><img class="scale-with-grid" src="content/app6/images/fitur4.png">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wrap mcb-wrap one-second valign-middle clearfix" style="padding:0 0 0 5%">
                            <div class="mcb-wrap-inner">
                                <div class="column mcb-column one column_column column-margin-20px">
                                    <div class="column_attr clearfix">
                                        <h5 class="app6-heading">Selling</h5>
                                        <h3 style="color: #0c6191;">Rekap Penjualan 
                                            <br>Lebih Lengkap</h3>
                                        <hr class="no_line" style="margin:0 auto 10px">
                                        <p>
                                            Memudahkan user menganalisa seluruh penjualan penjualan dan stok di toko, cabang, atau kantor pusat. Serta memudahkan user untuk menambahkan promo diskon dan
                                            sistem pengiriman. 
                                            <br>
                                            <br><a href="features.html" style="color: #319dcb;">Read more...</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="section mcb-section" id="features-5" style="padding-top:80px;">
                    <div class="section_wrapper mcb-section-inner">
                        <div class="wrap mcb-wrap one valign-top clearfix">
                            <div class="mcb-wrap-inner">
                                <div class="column mcb-column one column_column">
                                    <div class="column_attr clearfix">
                                        <h2 style="color: #0c6191; text-align: center;">Pilih Devicemu</h2>
                                    </div>
                                </div>
                                <div class="device-to" style="padding-left: 10% !important">
                                <div class="column mcb-column one-third column_list device" style="width: 20%;">
                                    <div class="list_item lists_3 clearfix">
                                        <div class="animate" data-anim-type="fadeIn">
                                            <div class="list_left list_image" style="margin:auto !important;"><img src="content/app6/images/dekstop.png" class="scale-with-grid">
                                            </div>
                                            <div class="list_right " style="text-align:center;">
                                                <h4 style="color: #319dcb;">Dekstop</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="column mcb-column one-third column_list device" style="width: 20%;">
                                    <div class="list_item lists_3 clearfix">
                                        <div class="animate" data-anim-type="fadeIn">
                                            <div class="list_left list_image" style="margin:auto !important;"><img src="content/app6/images/tablet.png" class="scale-with-grid">
                                            </div>
                                            <div class="list_right device-too" style="text-align:center;">
                                                <h4 style="color: #319dcb;">Tablet</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="column mcb-column one-third column_list device" style="width: 20%;">
                                    <div class="list_item lists_3 clearfix">
                                        <div class="animate" data-anim-type="fadeIn">
                                            <div class="list_left list_image" style="margin:auto !important;"><img src="content/app6/images/andropos.png" class="scale-with-grid">
                                            </div>
                                            <div class="list_right device-toooo" style="text-align:center;">
                                                <h4 style="color: #319dcb;">AndroidPOS</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="column mcb-column one-third column_list device" style="width: 20%;">
                                    <div class="list_item lists_3 clearfix">
                                        <div class="animate" data-anim-type="fadeIn">
                                            <div class="list_left list_image" style="margin:auto !important;"><img src="content/app6/images/phone.png" class="scale-with-grid">
                                            </div>
                                            <div class="list_right device-tooooo" style="text-align:center;">
                                                <h4 style="color: #319dcb;">Mobile</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="section mcb-section equal-height-wrap" id="features-2" style="margin-bottom:10%">
                    <div class="section_wrapper mcb-section-inner" >
                        <div class="column mcb-column one column_column" >
                            <div class="column_attr clearfix">
                                <h2 style="color: #0c6191; text-align: center; margin-bottom: 100px">Testimoni</h2>
                            </div>
                        </div>
                        <div class="wrap mcb-wrap one-second valign-middle clearfix" style="padding:0 5% 0 0">
                            <div class="mcb-wrap-inner">
                                <div class="column mcb-column one column_column column-margin-20px">
                                    <div class="column_attr clearfix">
                                        <h5 class="app6-heading">Glofriends</h5><br><br>
                                        <h4 style="color: #0c6191;"> "Glofin POS mampu mengatur manajemen toko secara efisien"</h4>
                                        <hr class="no_line" style="margin:0 auto 10px">
                                        <p style="font-size: 25px">
                                            - Kadek, Karyawati
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wrap mcb-wrap one-second valign-middle clearfix">
                            <div class="mcb-wrap-inner">
                                <div class="column mcb-column one column_image">
                                    <div class="animate" data-anim-type="fadeInUp">
                                        <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                                            <div class="image_wrapper">
                                                <video controls autoplay loop muted style="width: 100%; height: 80%;">
                                                    <source src="{{asset('My Video.webm')}}" type="video/webm"/>
                                                </video>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="section mcb-section bg-cover" id="app" style="padding-top:20px;background-position:center; margin-top: 40px; padding-bottom: 0%">
                    <div class="section mcb-section" id="features-5">
                        <div class="section_wrapper mcb-section-inner">
                            <div class="wrap mcb-wrap one valign-top clearfix">
                                <div class="mcb-wrap-inner">
                                    <div class="column mcb-column one column_column">
                                        <div class="column_attr clearfix">
                                            <h2 style="color: #0c6191; text-align: center;">Contact Us</h2>
                                        </div>
                                    </div>
                                    <div class="section mcb-section equal-height-wrap" id="features-1" style="padding-bottom:40px">
                                        <div class="section_wrapper mcb-section-inner">
                                            <div class="wrap mcb-wrap one-second valign-top clearfix">
                                                <div class="mcb-wrap-inner">
                                                    <div class="column mcb-column one column_image">
                                                        <div class="animate" data-anim-type="fadeInUp">
                                                            <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                                                                <div class="image_wrapper"><img class="scale-with-grid" src="content/app6/images/contact.png">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wrap mcb-wrap one-second valign-middle clearfix" style="padding:0 0 0 5%; margin-bottom: 0%;" >
                                                <div class="mcb-wrap-inner">
                                                    <div class="column mcb-column one column_column column-margin-20px">
                                                        <div class="column_attr clearfix" >
                                                            <div>
                                                                <div class="image_wrapper"><img class="scale-with-grid" src="content/app6/images/contact1.png">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection